(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/add-item/add-item.component.css":
/*!*************************************************!*\
  !*** ./src/app/add-item/add-item.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{\r\n    background-color: rgb(219, 219, 194)\r\n}\r\n\r\n.CategorySection, .PnameSection,.PriceSection,.btn-block{\r\n    margin-top: 30px;\r\n    margin: 15px auto;\r\n}\r\n\r\n.allinputs{\r\n   \r\n    height: 600px;\r\n    width: 80%;\r\n    margin-top: 25%;\r\n    -ms-grid-row-align: center;\r\n        align-self: center;\r\n    background-color: rgb(247, 247, 251);\r\n    border: 1px solid rgb(188, 164, 226);\r\n    border-radius: 2px;\r\n}\r\n\r\ninput{\r\n font-size: 25px;\r\n \r\n font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n}\r\n\r\n.inputfile{\r\n    margin-top: 30px;\r\n    width: 74%;\r\n}"

/***/ }),

/***/ "./src/app/add-item/add-item.component.html":
/*!**************************************************!*\
  !*** ./src/app/add-item/add-item.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"allinputs container-fluid\">\n         \n    \n\n <form [formGroup]=\"addItem\">  \n    <div class=\"PnameSection\">\n        <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Enter product name\" formControlName=\"Pname\" >\n        </mat-form-field>\n       \n    </div> \n    \n    <mat-divider></mat-divider>\n    <div class=\"CategorySection\">\n         <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Add Product Category\"  formControlName=\"Pcategory\" >\n          </mat-form-field>\n    </div>\n\n    <mat-divider></mat-divider>\n\n    <div class=\"PriceSection\">\n        <mat-form-field class=\"full-width\">\n            <input matInput placeholder=\"Add Price\" formControlName=\"Price\" >\n        </mat-form-field>\n    </div>\n\n\n    <mat-divider></mat-divider><br>\n    \n    <mat-divider></mat-divider>\n   \n        <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" formControlName=\"picPruduct\" class=\"inputfile\" />                                  \n                              \n                <br>\n </form>               \n     <button type=\"button\" class=\"btn btn-success btn-s\"\n                (click)=\"addProduct()\">\n            <span class=\"glyphicon glyphicon-upload\"></span> Upload </button><br>\n</div>"

/***/ }),

/***/ "./src/app/add-item/add-item.component.ts":
/*!************************************************!*\
  !*** ./src/app/add-item/add-item.component.ts ***!
  \************************************************/
/*! exports provided: AddItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddItemComponent", function() { return AddItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var uri = '/files/upload/';
var AddItemComponent = /** @class */ (function () {
    // isNaN('') not a number
    function AddItemComponent(dataService) {
        this.dataService = dataService;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_2__["FileUploader"]({ url: uri });
        this.addItem = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            Pname: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            Pcategory: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
            Price: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            picPruduct: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
    }
    AddItemComponent.prototype.ngOnInit = function () {
    };
    AddItemComponent.prototype.addProduct = function () {
        this.dataService.addProduct(this.addItem.value);
        this.uploader.uploadAll();
        location.reload();
    };
    AddItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-item',
            template: __webpack_require__(/*! ./add-item.component.html */ "./src/app/add-item/add-item.component.html"),
            styles: [__webpack_require__(/*! ./add-item.component.css */ "./src/app/add-item/add-item.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], AddItemComponent);
    return AddItemComponent;
}());



/***/ }),

/***/ "./src/app/admin-page/admin-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/admin-page/admin-page.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.allproducts{\r\n    border: 5px solid rgb(202, 212, 212);\r\n    background-color: azure;\r\n    border-radius: 5px;\r\n    \r\n \r\n    \r\n}\r\n.picitem{\r\n    width: 40px;\r\n    margin-left: 20%;\r\n    margin-top: 5px;\r\n   \r\n    \r\n}\r\n.product{\r\n\r\nborder: 1px solid brown;\r\nborder-radius: 5px;\r\nmargin: 5px;\r\nopacity: 0.8;\r\nheight: 150px;\r\ntext-align: center;\r\nwidth: 160px;\r\n\r\nalign-items: center;\r\n\r\n\r\n}\r\n.product:hover{\r\n    background-color: rgb(213, 226, 226);  \r\n   opacity: 1;\r\n\r\n}\r\n.picitem:hover{\r\n    width: 300px;\r\n    height: 300px;\r\n    position: relative;\r\n    z-index: 14;\r\n     \r\n}\r\n.picitem:hover ~ h4  {\r\n    display: none;\r\n}\r\n.picitem:hover ~ .itemPname{\r\n    display: none;\r\n}\r\n.addtocart{\r\n    margin-left: 5px;\r\n    border-radius: 5px;\r\n    background-color: rgb(117, 146, 207)\r\n}\r\n.cat{\r\n    background-color: #f8f8f8;\r\n    border: none;\r\n    border-left: 1px solid  rgb(211, 204, 204);\r\n    font-size: 20px;\r\n}\r\n.allBtn {\r\n    float: right;\r\n    cursor: pointer;\r\n    background-color: rgb(208, 218, 221);\r\n    border-radius:1px;\r\n    margin-top: 1px;\r\n    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n    text-shadow: 4px 4px 4px #aaa;\r\n}\r\n.addproductBtn{\r\n    float: right;\r\n    margin-top: 1px;\r\n    background-color: rgb(208, 218, 221);\r\n    border-radius:2px;\r\n    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n    font-size: 20px;\r\n}\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/admin-page/admin-page.component.html":
/*!******************************************************!*\
  !*** ./src/app/admin-page/admin-page.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container-fluid\">\n     \n    <div class=\"editView col-sm-6 col-md-4 col-lg-4\">\n      \n        <button class=\"addproductBtn\" (click)=\"tuggelAddBtn()\"><a routerLink=\"/add\"></a>Add Product</button>\n        <app-edited-item [idEdit]=\"sendId\" (messageEvent)=\"receive($event)\" *ngIf=\"tuggelEdit\"></app-edited-item>\n        <app-add-item  *ngIf=\"tuggelAdd\"></app-add-item>\n     </div>\n     \n  \n     <div class=\"allproducts col-sm-12 col-md-8 col-lg-8\">\n        <div class=\"categoryDiv\">\n            <nav class=\"navbar navbar-default\">\n                    <div class=\"container-fluid\">\n                      <div class=\"navbar-header\">\n                        <strong><span>Select Category :</span></strong><br> \n                      </div> \n                       <ul class=\"nav navbar-nav\">\n                         <li *ngFor=\"let cat of categories\"><button  class=\"cat\" [(ngModel)]=\"termCategory\" (click)=\"fillterCategory($event)\"  ngDefaultControl>{{cat.nameCategory}}</button></li>\n                      </ul>\n                     \n                    </div>\n                  </nav>\n                 <button (click)=\"resetSearch()\">reset</button> \n                  <div class=\"divsearch\">\n                     <span>Search for product</span><input type=\"text\" (focus)=\"resetSearch()\" [(ngModel)]=\"term\">\n                  </div>\n      \n\n    </div> \n                                                                        <!--  | filter:term |filterCategory:termCategory  -->\n                <div class=\"product col-sm-4 col-md-4 col-lg-4\" *ngFor=\"let item of dat | filter:term |filterCategory:termCategory\" >\n                <img class=\"picitem\" src=\"/assets/{{item.picProduct}}\">\n                <h4 class=\"itemPname\">{{item.productName}}</h4>\n                <h4 class=\"itemPname\">{{item.price | currency:'ILS':'symbol':'1.2-2'}}</h4>\n                \n                <button class=\"EditProduct itemPname\"(click)=\"editme(item)\">Edit Product</button>\n                \n                  \n            </div>\n     </div>\n \n</div>\n\n"

/***/ }),

/***/ "./src/app/admin-page/admin-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/admin-page/admin-page.component.ts ***!
  \****************************************************/
/*! exports provided: AdminPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminPageComponent", function() { return AdminPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminPageComponent = /** @class */ (function () {
    function AdminPageComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.tuggelEdit = false;
        this.tuggelAdd = false;
        this.dat = [];
        this.sendId = "";
        this.categories = [];
        this.currentUser = "";
    }
    AdminPageComponent.prototype.receive = function ($event) {
        var _this = this;
        console.log($event);
        if ($event == "updateView") {
            this.categories = [];
            this.dat = [];
            return this.dataService.getCategories()
                .subscribe(function (data) {
                _this.categories = data;
                _this.dataService.allProducts()
                    .subscribe(function (data) {
                    _this.dat = data;
                });
            });
        }
    };
    AdminPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.check()
            .subscribe(function (data) {
            if (data == "not loged") {
                // console.log(data)
                _this.router.navigate(['/login']);
            }
            _this.currentUser = data;
            // console.log(data)
        });
        this.dataService.getCategories()
            .subscribe(function (data) {
            _this.categories = data;
        });
        this.dataService.allProducts()
            .subscribe(function (data) {
            _this.dat = data;
        });
    };
    AdminPageComponent.prototype.fillterCategory = function (category) {
        this.termCategory = category.target.textContent;
    };
    AdminPageComponent.prototype.resetSearch = function () {
        this.termCategory = "";
    };
    AdminPageComponent.prototype.filterBy = function (e) {
    };
    AdminPageComponent.prototype.editme = function (e) {
        var _this = this;
        this.dat = [];
        this.dataService.allProducts()
            .subscribe(function (data) {
            _this.dat = data;
        });
        this.sendId = e;
        this.tuggelAdd = false;
        this.tuggelEdit = true;
        return e;
    };
    AdminPageComponent.prototype.tuggelAddBtn = function () {
        this.tuggelEdit = false;
        this.tuggelAdd = true;
    };
    AdminPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-page',
            template: __webpack_require__(/*! ./admin-page.component.html */ "./src/app/admin-page/admin-page.component.html"),
            styles: [__webpack_require__(/*! ./admin-page.component.css */ "./src/app/admin-page/admin-page.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AdminPageComponent);
    return AdminPageComponent;
}());



/***/ }),

/***/ "./src/app/admin-page/protect-gauard.service.ts":
/*!******************************************************!*\
  !*** ./src/app/admin-page/protect-gauard.service.ts ***!
  \******************************************************/
/*! exports provided: ProtectGauardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProtectGauardService", function() { return ProtectGauardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProtectGauardService = /** @class */ (function () {
    function ProtectGauardService(http, router) {
        this.http = http;
        this.router = router;
    }
    ProtectGauardService.prototype.canActivate = function () {
        this.check = this.http.get("/users/protectadmin");
        if (this.check == false) {
            this.router.navigate(["/"]);
        }
        return this.check;
    };
    ProtectGauardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ProtectGauardService);
    return ProtectGauardService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<app-header></app-header>\n<div class=\"container-fluid\">\n\n        <router-outlet></router-outlet>\n\n\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import {HttpModule} from '@angular/http';
// import {Http} from '@angular/http';
// import {HttpClientModule} from '@angular/common/http';
// import {HttpClient} from '@angular/common/http';
// import {DataService} from './data.service';
// import {TableComponent} from './table/table.component'
// import {HeaderComponent} from './header/header.component'
// import {RegisterationComponent} from  './registeration/registeration.component'

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    ;
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _table_table_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./table/table.component */ "./src/app/table/table.component.ts");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./data.service */ "./src/app/data.service.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _log_in_page_log_in_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./log-in-page/log-in-page.component */ "./src/app/log-in-page/log-in-page.component.ts");
/* harmony import */ var _registeration_registeration_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./registeration/registeration.component */ "./src/app/registeration/registeration.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _shop_main_shop_main_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shop-main/shop-main.component */ "./src/app/shop-main/shop-main.component.ts");
/* harmony import */ var _cart_section_cart_section_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./cart-section/cart-section.component */ "./src/app/cart-section/cart-section.component.ts");
/* harmony import */ var _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./admin-page/admin-page.component */ "./src/app/admin-page/admin-page.component.ts");
/* harmony import */ var _admin_page_protect_gauard_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./admin-page/protect-gauard.service */ "./src/app/admin-page/protect-gauard.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _edited_item_edited_item_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./edited-item/edited-item.component */ "./src/app/edited-item/edited-item.component.ts");
/* harmony import */ var _filter_pipe__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./filter.pipe */ "./src/app/filter.pipe.ts");
/* harmony import */ var _add_item_add_item_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./add-item/add-item.component */ "./src/app/add-item/add-item.component.ts");
/* harmony import */ var _filterCategory_pipe__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./filterCategory.pipe */ "./src/app/filterCategory.pipe.ts");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _select_select_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./select/select.component */ "./src/app/select/select.component.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./product-detail/product-detail.component */ "./src/app/product-detail/product-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




































var appRoutes = [
    { path: "register", component: _registeration_registeration_component__WEBPACK_IMPORTED_MODULE_10__["RegisterationComponent"] },
    { path: "login", component: _log_in_page_log_in_page_component__WEBPACK_IMPORTED_MODULE_9__["LogInPageComponent"] },
    { path: "product/:id", component: _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_35__["ProductDetailComponent"] },
    { path: "", component: _log_in_page_log_in_page_component__WEBPACK_IMPORTED_MODULE_9__["LogInPageComponent"] },
    { path: "admin",
        canActivate: [_admin_page_protect_gauard_service__WEBPACK_IMPORTED_MODULE_15__["ProtectGauardService"]],
        component: _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_14__["AdminPageComponent"] },
    { path: "add", component: _add_item_add_item_component__WEBPACK_IMPORTED_MODULE_20__["AddItemComponent"] },
    { path: "shopmain", component: _table_table_component__WEBPACK_IMPORTED_MODULE_5__["TableComponent"] },
    { path: "order", component: _order_order_component__WEBPACK_IMPORTED_MODULE_28__["OrderComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _table_table_component__WEBPACK_IMPORTED_MODULE_5__["TableComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_8__["HeaderComponent"],
                _log_in_page_log_in_page_component__WEBPACK_IMPORTED_MODULE_9__["LogInPageComponent"],
                _registeration_registeration_component__WEBPACK_IMPORTED_MODULE_10__["RegisterationComponent"],
                _shop_main_shop_main_component__WEBPACK_IMPORTED_MODULE_12__["ShopMainComponent"],
                _cart_section_cart_section_component__WEBPACK_IMPORTED_MODULE_13__["CartSectionComponent"],
                _admin_page_admin_page_component__WEBPACK_IMPORTED_MODULE_14__["AdminPageComponent"],
                _edited_item_edited_item_component__WEBPACK_IMPORTED_MODULE_18__["EditedItemComponent"],
                _add_item_add_item_component__WEBPACK_IMPORTED_MODULE_20__["AddItemComponent"],
                _filter_pipe__WEBPACK_IMPORTED_MODULE_19__["FilterPipe"],
                _filterCategory_pipe__WEBPACK_IMPORTED_MODULE_21__["FilterCategoryPipe"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__["FileSelectDirective"],
                _order_order_component__WEBPACK_IMPORTED_MODULE_28__["OrderComponent"],
                _select_select_component__WEBPACK_IMPORTED_MODULE_34__["SelectComponent"],
                _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_35__["ProductDetailComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_22__["MatCheckboxModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_23__["MatInputModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_25__["MatDividerModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_24__["MatButtonModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__["MatDialogModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_27__["MatSelectModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__["MatDialogModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_29__["MatListModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_30__["MatIconModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_32__["MatDatepickerModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_31__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_33__["MatNativeDateModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_26__["MatDialogModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_16__["RouterModule"].forRoot(appRoutes)
            ],
            entryComponents: [
                _order_order_component__WEBPACK_IMPORTED_MODULE_28__["OrderComponent"]
            ],
            providers: [_data_service__WEBPACK_IMPORTED_MODULE_7__["DataService"], _admin_page_protect_gauard_service__WEBPACK_IMPORTED_MODULE_15__["ProtectGauardService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/cart-section/cart-section.component.css":
/*!*********************************************************!*\
  !*** ./src/app/cart-section/cart-section.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".HeadTitle{\r\n    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n    text-shadow: 4px 4px 4px #aaa;\r\n    border-bottom: 1px solid cornflowerblue;\r\n    \r\n}\r\n.picitem{\r\n    height: 40px;\r\n    width: 40px;\r\n\r\n}\r\n"

/***/ }),

/***/ "./src/app/cart-section/cart-section.component.html":
/*!**********************************************************!*\
  !*** ./src/app/cart-section/cart-section.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>\n  cart-section works!\n  {{userCart}}\n</p> -->\n<div class=\"container-fluid\">\n  <div class=\"col-sm-12 col-ms-12 col-lg-12\">\n      \n      <h2 class=\"HeadTitle\" >My Cart</h2>\n      <button id=\"remove\" class=\"btn btn-warning\" (click)=\"!confirmEmptyCart ? applyDel($event):cancelDeleteAll()\">Remove All Products</button><button  class=\"btn btn-danger glyphicon glyphicon-remove\" *ngIf=\"confirmEmptyCart\" (click)=\"removeAll(userCart)\"> Clear All</button>\n        \n        <div class=\"table-responsive\">\n\n\n            <table class=\"table\">\n                <thead>\n                    <tr>\n                      <th scope=\"col\">product</th>\n                      <th scope=\"col\">Product name</th>\n                      <th scope=\"col\">Quantity</th>\n                      <th scope=\"col\">Price</th>\n                      <th scope=\"col\"></th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                      <tr *ngFor=\"let item of cart\">\n                        <td><img class=\"picitem\" src=\"/assets/{{item.cartItemPic}}\"></td>\n                         <td> {{item.productName}}</td>\n                         <td>{{item.amount}}</td>\n                         <td>{{item.totalPrice}}</td>\n                         <td><button class=\"glyphicon glyphicon-remove\" (click)=\"removeitem(item._id)\"> Order</button></td>\n                      </tr>\n                </tbody>\n            </table>\n           \n            \n            \n          </div>\n          <button mat-raised-button   matTooltip=\"Continue to Order\" (click)=\"openDialog()\" color=\"primary\" *ngIf=\"cart && cart.length\">Order</button>\n\n  </div>\n  \n\n\n\n\n\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/cart-section/cart-section.component.ts":
/*!********************************************************!*\
  !*** ./src/app/cart-section/cart-section.component.ts ***!
  \********************************************************/
/*! exports provided: CartSectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartSectionComponent", function() { return CartSectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _order_order_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../order/order.component */ "./src/app/order/order.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CartSectionComponent = /** @class */ (function () {
    function CartSectionComponent(dataService, dialog, router) {
        this.dataService = dataService;
        this.dialog = dialog;
        this.router = router;
        this.cart = [];
        this.confirmEmptyCart = false;
    }
    CartSectionComponent.prototype.ngOnInit = function () {
    };
    CartSectionComponent.prototype.ngOnChanges = function () {
        var _this = this;
        this.dataService.getCart(this.userCart)
            .subscribe(function (data) {
            _this.cart = data;
            _this.userOrder = _this.userCart;
        });
        this.cart.push(this.idEdit);
    };
    CartSectionComponent.prototype.applyDel = function (e) {
        this.confirmEmptyCart = true;
        e.target.textContent = "Cancel";
        console.log(e);
    };
    CartSectionComponent.prototype.openDialog = function () {
        var _this = this;
        this.dataService.check()
            .subscribe(function (data) {
            if (data == "not loged") {
                console.log(data);
                _this.router.navigate(['/login']);
            }
            else {
                var dialogRef = _this.dialog.open(_order_order_component__WEBPACK_IMPORTED_MODULE_3__["OrderComponent"], {
                    width: '80%',
                    height: "600px",
                });
                dialogRef.afterClosed().subscribe(function (result) {
                    _this.cart = [];
                });
            }
        });
    };
    CartSectionComponent.prototype.removeitem = function (id) {
        var _this = this;
        this.dataService.removeItemFromCart(id)
            .subscribe(function (data) {
            _this.dataService.getCart(_this.userCart)
                .subscribe(function (data) {
                _this.cart = data;
            });
        });
    };
    CartSectionComponent.prototype.cancelDeleteAll = function () {
        document.getElementById("remove").innerHTML = "Remove All Products";
        this.confirmEmptyCart = false;
    };
    CartSectionComponent.prototype.removeAll = function (user) {
        var _this = this;
        this.dataService.removeAllFromCart(user)
            .subscribe(function (data) {
            _this.dataService.getCart(_this.userCart)
                .subscribe(function (data) {
                document.getElementById("remove").innerHTML = "Remove All Products";
                _this.confirmEmptyCart = false;
                _this.cart = data;
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CartSectionComponent.prototype, "idEdit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CartSectionComponent.prototype, "userCart", void 0);
    CartSectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cart-section',
            template: __webpack_require__(/*! ./cart-section.component.html */ "./src/app/cart-section/cart-section.component.html"),
            styles: [__webpack_require__(/*! ./cart-section.component.css */ "./src/app/cart-section/cart-section.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], CartSectionComponent);
    return CartSectionComponent;
}());



/***/ }),

/***/ "./src/app/data.service.ts":
/*!*********************************!*\
  !*** ./src/app/data.service.ts ***!
  \*********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { all } from 'q';
var DataService = /** @class */ (function () {
    function DataService(http, router) {
        this.http = http;
        this.router = router;
        this.userToComponent = "";
    }
    ;
    DataService.prototype.addProduct = function (newProduct) {
        console.log(newProduct);
        this.http.post("/api/createProduct", { newProduct: newProduct })
            .subscribe(function (data) {
        });
    };
    DataService.prototype.invoice = function (dat) {
        return this.http.post('/files/creatInvoice', { dat: dat })
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.editProduct = function (newProduct) {
        return this.http.put("/api/editProduct", newProduct)
            .map(function (data) {
            return (data);
            // console.log(data)
        });
    };
    DataService.prototype.check = function () {
        return this.http.get("/users/auth")
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.getUserData = function (username) {
        return this.http.get("/users/getUserData/" + username, { responseType: 'json' })
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.allProducts = function () {
        return this.http.get("/api/getAllProducts")
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.getCategories = function () {
        return this.http.get("/api/getCategories")
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.getNumOfProducts = function () {
        return this.http.get("/api/numAllProducts")
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.getNumOfOrders = function () {
        return this.http.get("/api/numorders")
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.getCart = function (name) {
        return this.http.get("/api/getCurrentCart/" + name, { responseType: 'json' })
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.removeItemFromCart = function (id) {
        var url = "/api/removeItemFromCart/" + id;
        return this.http.delete(url, { responseType: 'text' })
            .map(function (response) {
            return response;
        });
    };
    DataService.prototype.removeAllFromCart = function (user) {
        var url = "/api/removeAllFromCart/" + user;
        return this.http.delete(url, { responseType: 'text' })
            .map(function (response) {
            return response;
        });
    };
    DataService.prototype.checkIfCart = function (name) {
        var url = "/api/checkifcart/" + name;
        return this.http.get(url, { responseType: 'text' })
            .map(function (response) {
            return response;
        });
    };
    DataService.prototype.checkIfOrder = function (name) {
        var url = "/api/checkifOrder/" + name;
        return this.http.get(url, { responseType: 'text' })
            .map(function (response) {
            return response;
        });
    }; //
    DataService.prototype.getcartItems = function (name) {
        var url = "/api/getcartItems/" + name;
        return this.http.get(url, { responseType: 'text' })
            .map(function (response) {
            return response;
        });
    }; //
    DataService.prototype.makeCart = function (newCart) {
        this.http.post("/api/addCart", { name: newCart })
            .subscribe(function (data) {
        });
    };
    DataService.prototype.makeCartItem = function (newCartItem) {
        this.http.post("/api/addItemToCart", { newCartItem: newCartItem })
            .subscribe(function (data) {
        });
    };
    DataService.prototype.register = function (newuser) {
        var _this = this;
        return this.http.post("users/register", newuser)
            .map(function (data) {
            if (data.msg == "user already exist") {
                return data.msg;
            }
            else {
                _this.router.navigate(['/login']);
            }
        });
    };
    DataService.prototype.login = function (currenuser) {
        return this.http.post("/users/login", currenuser, { responseType: 'text' })
            .map(function (response) {
            return { response: response };
        });
    };
    DataService.prototype.logOut = function () {
        var _this = this;
        return this.http.get("/users/logout", { responseType: 'text' })
            .map(function (response) {
            _this.router.navigate(['/login']);
        });
    };
    DataService.prototype.getSumOfOrder = function (name) {
        return this.http.get("/api/getsumoforder/" + name, { responseType: 'json' })
            .map(function (data) {
            return data;
        });
    };
    DataService.prototype.submitOrder = function (order) {
        return this.http.post("/api/makeorder", order, { responseType: 'json' })
            .map(function (data) {
            return data;
        });
    };
    DataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/edited-item/edited-item.component.css":
/*!*******************************************************!*\
  !*** ./src/app/edited-item/edited-item.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{\r\n    background-color: rgb(165, 165, 112)\r\n}\r\n\r\n.CategorySection, .PnameSection,.PriceSection{\r\n    margin-top: 50px;\r\n    margin: 50px auto;\r\n}\r\n\r\n.allinputs{\r\n   \r\n    height: 440px;\r\n    width: 80%;\r\n    margin-top: 25%;\r\n    -ms-grid-row-align: center;\r\n        align-self: center;\r\n    background-color: rgb(240, 183, 183);\r\n    border: 1px solid rgb(188, 164, 226);\r\n    border-radius: 2px;\r\n}\r\n\r\ninput{\r\n font-size: 25px;\r\n border: 1px solid rgb(119, 106, 141);\r\n font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n \r\n\r\n\r\n\r\n}"

/***/ }),

/***/ "./src/app/edited-item/edited-item.component.html":
/*!********************************************************!*\
  !*** ./src/app/edited-item/edited-item.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>\n  {{idEdit.productName}}\n</p> -->\n\n<div class=\"allinputs container-fluid\">\n         \n    <div class=\"PnameSection\">\n        <label for=\"Pname\"> Edit Product Name</label>\n        <input type=\"text\"  id=\"Pname\" value=\"{{idEdit.productName}}\" [(ngModel)]=\"idEdit.productName\" >\n       \n    </div>\n\n    <div class=\"CategorySection\">\n        <label for=\"Pcategory\">Edit Product Category</label>\n        <input type=\"text\"  id=\"Pcategory\" value=\"{{idEdit.categoryId}}\" [(ngModel)]=\"idEdit.categoryId\">\n        \n        \n    </div>\n\n    <div class=\"PriceSection\">\n        <label for=\"Pprice\">Edit Price</label>\n        <input type=\"text\"  id=\"Pprice\" value=\"{{idEdit.price}}\" [(ngModel)]=\"idEdit.price\"><small><span>ש'ח</span></small>\n    </div>\n    <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"update()\">Update</button>\n\n</div>\n\n\n\n  \n    \n\n       \n\n        \n       \n        \n   \n    \n\n\n            \n   \n\n  \n"

/***/ }),

/***/ "./src/app/edited-item/edited-item.component.ts":
/*!******************************************************!*\
  !*** ./src/app/edited-item/edited-item.component.ts ***!
  \******************************************************/
/*! exports provided: EditedItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditedItemComponent", function() { return EditedItemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditedItemComponent = /** @class */ (function () {
    function EditedItemComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.messageEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.updateView = "updateView";
    }
    EditedItemComponent.prototype.ngOnInit = function () {
    };
    EditedItemComponent.prototype.update = function () {
        var _this = this;
        //  let update={id:this.idEdit._id , productName:this.idEdit.productName , categoryId:this.idEdit.categoryId , price:this.idEdit.price}
        console.log(this.idEdit);
        return this.dataService.editProduct(this.idEdit)
            .subscribe(function (data) {
            _this.messageEvent.emit(_this.updateView);
            _this.idEdit = {};
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], EditedItemComponent.prototype, "idEdit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], EditedItemComponent.prototype, "messageEvent", void 0);
    EditedItemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edited-item',
            template: __webpack_require__(/*! ./edited-item.component.html */ "./src/app/edited-item/edited-item.component.html"),
            styles: [__webpack_require__(/*! ./edited-item.component.css */ "./src/app/edited-item/edited-item.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EditedItemComponent);
    return EditedItemComponent;
}());



/***/ }),

/***/ "./src/app/filter.pipe.ts":
/*!********************************!*\
  !*** ./src/app/filter.pipe.ts ***!
  \********************************/
/*! exports provided: FilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterPipe", function() { return FilterPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (dat, term) {
        if (term === undefined)
            return dat;
        return dat.filter(function (product) {
            return product.productName.toLowerCase().includes(term.toLowerCase());
        });
    };
    FilterPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());



/***/ }),

/***/ "./src/app/filterCategory.pipe.ts":
/*!****************************************!*\
  !*** ./src/app/filterCategory.pipe.ts ***!
  \****************************************/
/*! exports provided: FilterCategoryPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterCategoryPipe", function() { return FilterCategoryPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FilterCategoryPipe = /** @class */ (function () {
    function FilterCategoryPipe() {
    }
    FilterCategoryPipe.prototype.transform = function (dat, termCategory) {
        if (termCategory === undefined)
            return dat;
        return dat.filter(function (products) {
            return products.categoryId.includes(termCategory);
        });
    };
    FilterCategoryPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'filterCategory'
        })
    ], FilterCategoryPipe);
    return FilterCategoryPipe;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar {\r\n    margin-bottom: 50px;\r\n    border-radius: 0;\r\n  }\r\n  \r\n  /* Remove the jumbotron's default bottom margin */\r\n  \r\n  .jumbotron {\r\n    margin-bottom: 0;\r\n    height: 150px;\r\n  }\r\n  \r\n  .headLogo{\r\n    height: 200px;\r\n    width: 200px;\r\n  }\r\n  \r\n  .logoff{\r\n    float: right;\r\n  }"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\n    <div class=\"container text-center\">\n      <h1><img width=\"50\" alt=\"Angular Logo\" src={{logo}}/>Online Store</h1>      \n    </div>\n</div>\n<button type=\"button\" (click)=\"logof()\" class=\"logoff btn btn-default btn-sm\">\n    <span class=\"glyphicon glyphicon-log-out\"></span> Log out\n  </button>\n  \n  \n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { LogInPageComponent } from '../log-in-page/log-in-page.component';

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(dataService) {
        this.dataService = dataService;
        this.logo = "../assets/header/mainBarImage.png";
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.logof = function () {
        this.dataService.logOut()
            .subscribe(function (data) {
        });
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/log-in-page/log-in-page.component.css":
/*!*******************************************************!*\
  !*** ./src/app/log-in-page/log-in-page.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(http://fonts.googleapis.com/css?family=Roboto);\r\n\r\n/****** LOGIN MODAL ******/\r\n\r\n.loginmodal-container {\r\n  padding: 30px;\r\n  max-width: 350px;\r\n  width: 100% !important;\r\n  background-color: #F7F7F7;\r\n  margin: 0 auto;\r\n  border-radius: 2px;\r\n  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n  overflow: hidden;\r\n  font-family: roboto;\r\n}\r\n\r\n.loginmodal-container h1 {\r\n  text-align: center;\r\n  font-size: 1.8em;\r\n  font-family: roboto;\r\n}\r\n\r\n.loginmodal-container input[type=submit] {\r\n  width: 100%;\r\n  display: block;\r\n  margin-bottom: 10px;\r\n  position: relative;\r\n}\r\n\r\n.loginmodal-container input[type=text], input[type=password] {\r\n  /* height: 44px;\r\n  font-size: 16px;\r\n  width: 100%; */\r\n  margin-bottom: 10px;\r\n  /* -webkit-appearance: none;\r\n  background: #fff;\r\n  border: 1px solid #d9d9d9;\r\n  border-top: 1px solid #c0c0c0;\r\n \r\n  padding: 0 8px; */\r\n  box-sizing: border-box;\r\n \r\n}\r\n\r\n/* .loginmodal-container input[type=text]:hover, input[type=password]:hover {\r\n  border: 1px solid #b9b9b9;\r\n  border-top: 1px solid #a0a0a0;\r\n  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);\r\n  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);\r\n  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);\r\n} */\r\n\r\n.loginmodal {\r\n  text-align: center;\r\n  font-size: 14px;\r\n  font-family: 'Arial', sans-serif;\r\n  font-weight: 700;\r\n  height: 36px;\r\n  padding: 0 8px;\r\n/* border-radius: 3px; */\r\n/* -webkit-user-select: none;\r\n  user-select: none; */\r\n}\r\n\r\n.loginmodal-submit {\r\n  /* border: 1px solid #3079ed; */\r\n  border: 0px;\r\n  color: #fff;\r\n  text-shadow: 0 1px rgba(0,0,0,0.1); \r\n  background-color: #4d90fe;\r\n  padding: 17px 0px;\r\n  font-family: roboto;\r\n  font-size: 14px;\r\n  width: 100%;\r\n  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */\r\n}\r\n\r\n.continueShopping {\r\n  \r\n  border: 0px;\r\n  color: #fff;\r\n  text-shadow: 0 1px rgba(0,0,0,0.1); \r\n  /* background-color: #fff; */\r\n  padding: 17px 0px;\r\n  font-family: roboto;\r\n  font-size: 14px;\r\n  margin-top: 10px;\r\n  \r\n}\r\n\r\n/* loginmodal-submit */\r\n\r\n.continueShoppingDiabled{\r\n  background-color: #fff;\r\n}\r\n\r\n/* .loginmodal-submit:hover {\r\n \r\n  border: 0px;\r\n  text-shadow: 0 1px rgba(0,0,0,0.3);\r\n  background-color: #357ae8;\r\n \r\n} */\r\n\r\n.loginmodal-container a {\r\n  text-decoration: none;\r\n  color: #666;\r\n  font-weight: 400;\r\n  text-align: center;\r\n  display: inline-block;\r\n  opacity: 0.8;\r\n  transition: opacity ease 0.5s;\r\n}\r\n\r\n.login-help{\r\n  \r\n  font-size: 12px;\r\n}\r\n\r\n.userdisplyname{\r\n  background-color: rgb(226, 255, 254);\r\n}\r\n\r\n.usermsgdiv{\r\n  \r\n  float: right;\r\n}\r\n\r\n.usermsg{\r\n  font-size: 20px;\r\n}\r\n\r\n.userMsgAfterLog{\r\nfont-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;\r\ncolor: rgb(11, 65, 165);\r\n}"

/***/ }),

/***/ "./src/app/log-in-page/log-in-page.component.html":
/*!********************************************************!*\
  !*** ./src/app/log-in-page/log-in-page.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\t<div class=\"container-fluid\">\n\t\t\t<div class=\"userdisplyname col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n\t\t\t\t<div class=\"usermsgdiv\">\n\t\t\t\t  <span class=\"usermsg\" >wellcom :</span><span class=\"usermsg\">{{userdata.name}}</span>\n\t\t\t\t</div>\n\t\t\t</div>\n\t<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n\t\t\t<h1>Login to Your Account</h1><br>\n\t\t\t<h4 class=\"userMsgAfterLog\" [(ngModel)]=\"MsgAfterLog\" ngDefaultControl>{{MsgAfterLog}}</h4>\n\t\t\t\n\t\t\t<mat-divider></mat-divider>\n\t\t\t<form [formGroup]=\"loginForm\">\n\t\t\t\t<mat-form-field class=\"example-full-width\"> <input formControlName=\"username\" matInput placeholder=\"User name\" type=\"text\"></mat-form-field><br>\n\t\t\t\t<mat-form-field class=\"example-full-width\"> <input formControlName=\"password\" matInput placeholder=\"Password\" type=\"password\"></mat-form-field>\n\n\t\t\t\t\n\t\t\t\t<button mat-raised-button (click)=\"login()\"  class=\"login loginmodal-submit\">Login</button>\n\t\t\t\t\n\t\t\t\t<button mat-raised-button (click)=\"continue()\"    [disabled]=\"buttonStatus\" [ngClass]=\"currentClass\">Continue shopping</button>\n                \n\t\t\t\t\n\t\t\t</form>\n\t\n\t\t\t\t<div class=\"login-help\">\n\t\t\t\t   <span>Don't Have an Account?       </span><a routerLink=\"/register\">Register</a> \n\t   \t\t    </div>\n\t</div>\t\n\t<div class=\"about col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n\t\t\t<h1>About the shop</h1><br>\n\t\t\t<article>\n\t\t\t\tThe Best Way to shop on line<br>\n\t\t\t\t\n\t\t\t\tComing soon..\n\n\t\t\t\t\n\t\t\t</article>\n\t\n\t</div>\t\n\t<div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n\t\t\t<h1>Information</h1><br>\n\n\n\t<marquee direction=\"up\" height=\"300px\">\n\t\t\n\t\t<h4> Currently we have,</h4>\n\t\t<h4 [(ngModel)]=\"numProducts\" ngDefaultControl><span style=\"color:red\">{{numProducts}} </span>  products.</h4>\n\t\t<h4 style=\"margin-top: 20px;\"> For the present,</h4>\n\t\t<h4 [(ngModel)]=\"numOrders\" ngDefaultControl> we have <span style=\"color:red;\">{{numOrders}}</span> Orders .</h4>\n\n\t</marquee>\n\n\t\t\n\t\n\t</div>\t\n\t\n\t</div>\n\n\n"

/***/ }),

/***/ "./src/app/log-in-page/log-in-page.component.ts":
/*!******************************************************!*\
  !*** ./src/app/log-in-page/log-in-page.component.ts ***!
  \******************************************************/
/*! exports provided: LogInPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogInPageComponent", function() { return LogInPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LogInPageComponent = /** @class */ (function () {
    function LogInPageComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.check = "";
        this.userdata = "";
        this.continueStyle = "#fff";
        this.buttonStatus = true;
        this.currentClass = "continueShoppingDiabled loginmodal-submit";
        this.numProducts = "";
        this.numOrders = "";
        this.MsgAfterLog = "";
        this.sumCurentCart = 0;
        this.dateFoundCart = "";
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
    }
    LogInPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getNumOfProducts()
            .subscribe(function (data) {
            _this.numProducts = data;
        });
        this.dataService.getNumOfOrders()
            .subscribe(function (data) {
            _this.numOrders = data;
        });
    };
    LogInPageComponent.prototype.login = function () {
        var _this = this;
        this.dataService.login(this.loginForm.value)
            .subscribe(function (data) {
            if (JSON.parse(data.response) == "invalid user/password") {
                _this.MsgAfterLog = JSON.parse(data.response);
                console.log(JSON.parse(data.response));
            }
            else {
                _this.userdata = JSON.parse(data.response);
                if (_this.userdata.roll == 1) {
                    _this.router.navigate(['/admin']);
                }
                else {
                    //check if active cart
                    _this.dataService.checkIfCart(_this.loginForm.controls.username.value)
                        .subscribe(function (data) {
                        data = JSON.parse(data);
                        //case if  active cart 
                        if (data.length != 0) {
                            _this.dateFoundCart = data[0].shoppingCarDate;
                            _this.dateFoundCart = _this.dateFoundCart.split(" ", 4).toString().replace(",", " ");
                            //console.log(this.dateFoundCart)
                            _this.dataService.getcartItems(_this.loginForm.controls.username.value)
                                .subscribe(function (response) {
                                response = JSON.parse(response);
                                _this.sumCurentCart = 0;
                                for (var i = 0; i < response.length; i++) {
                                    _this.sumCurentCart += response[i].totalPrice;
                                }
                                _this.buttonStatus = false;
                                _this.currentClass = "continueShopping loginmodal-submit";
                                _this.MsgAfterLog = "welcome back ,you have a cart from  \n" + _this.dateFoundCart + "  with the sum " + Math.round(_this.sumCurentCart * 100) / 100 + " שח";
                            });
                        }
                        else {
                            _this.dataService.checkIfOrder(_this.loginForm.controls.username.value)
                                .subscribe(function (orderdata) {
                                if (orderdata.length == 0) {
                                    _this.MsgAfterLog = "Welcome  to your first  buy";
                                    _this.buttonStatus = false;
                                    _this.currentClass = "continueShopping loginmodal-submit";
                                }
                                else {
                                    _this.MsgAfterLog = "Your last order was at , " + JSON.parse(orderdata);
                                    _this.buttonStatus = false;
                                    _this.currentClass = "continueShopping loginmodal-submit";
                                }
                            });
                        }
                    });
                }
            }
        });
    };
    LogInPageComponent.prototype.continue = function () {
        var _this = this;
        this.dataService.checkIfCart(this.loginForm.controls.username.value)
            .subscribe(function (data) {
            data = JSON.parse(data);
            // console.log(data.length,this.loginForm.controls.username.value)
            if (data.length == 0) {
                _this.dataService.makeCart(_this.loginForm.controls.username.value);
                _this.router.navigate(['/shopmain']);
            }
            else {
                _this.router.navigate(['/shopmain']);
            }
        });
    };
    LogInPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-log-in-page',
            template: __webpack_require__(/*! ./log-in-page.component.html */ "./src/app/log-in-page/log-in-page.component.html"),
            styles: [__webpack_require__(/*! ./log-in-page.component.css */ "./src/app/log-in-page/log-in-page.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], LogInPageComponent);
    return LogInPageComponent;
}());



/***/ }),

/***/ "./src/app/order/order.component.css":
/*!*******************************************!*\
  !*** ./src/app/order/order.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".products{\r\n  float: left;\r\n  height: 500px;\r\n  overflow: scroll;\r\n}\r\n.userdata{\r\n   \r\n    float: right;\r\n   \r\n}\r\ntable{\r\n    margin-top: 70px;\r\n    z-index: 0;\r\n   \r\n}\r\n.Shippingdetail{\r\n    margin-top: 20px;\r\n}\r\n.orderheader{\r\n    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;  \r\n}\r\n.listtitle{\r\n   \r\n  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;  \r\n  z-index: 1000;\r\n  position: fixed;\r\n  background-color: rgb(245, 242, 241);\r\n}\r\n.tomato{\r\n    background-color: tomato;\r\n}\r\n.divider{\r\n    width: 50%;\r\n}\r\n.submitbtn{\r\n    margin-right: 20px;\r\n}\r\n.errmsg{\r\n    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;   \r\n}\r\n.msgerr{\r\n   height: 70px;\r\n   background-color: aliceblue;\r\n   width: 250px;\r\n}\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/order/order.component.html":
/*!********************************************!*\
  !*** ./src/app/order/order.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <div class=\"container\">\n\n\n  <div class=\"col-sm-12 col-md-4 col-lg-4 products \"> \n    <div class=\"listtitle\">\n        <h1 > Products in the Cart</h1>\n        <h3 class=\"sumOrder\" [(ngModel)]=\"sumOfOrder\" ngDefaultControl> Total:   {{sumOfOrder}}<small><span>שח</span></small></h3>\n        <mat-divider class=\"tomato\"></mat-divider>\n\n    </div>\n    \n       <table class=\"table-responsive table-bordered table-condensed\">\n          <thead>\n              <tr>\n                <th>product</th>\n                <th >Product name</th>\n                <th >Quantity</th>\n                <th  >Price</th>\n                \n              </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let item of cart\">\n                  <td><img class=\"picitem\" src=\"/assets/{{item.cartItemPic}}\" width=\"40\" height=\"40\"></td>\n                   <td> {{item.productName}}</td>\n                   <td>{{item.amount}}</td>\n                   <td>{{item.totalPrice}}</td>\n                </tr>\n          </tbody>\n      </table> \n    \n  \n     \n  </div>\n  \n  <div class=\"col-sm-12 col-md-8 col-lg-8 userdata\">\n    \n    <h1 class=\"orderheader\">Shipping details</h1>\n    <mat-divider class=\"tomato\"></mat-divider>\n    <div class=\"Shippingdetail\" (click)=\"getUserData()\">\n        <form [formGroup]=\"order\"> \n            <mat-form-field>\n              <input matInput placeholder=\"City\" formControlName=\"city\" >\n            </mat-form-field><br>\n            <mat-divider class=\"divider\"></mat-divider>\n          \n            <mat-form-field>\n                <input matInput placeholder=\"street\" formControlName=\"street\" >\n            </mat-form-field><br>\n            <mat-divider class=\"divider\"></mat-divider>\n          \n            <mat-form-field>\n                <input matInput [matDatepicker]=\"picker\" placeholder=\"shipping Date\" formControlName=\"date\" >\n                <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n                <mat-datepicker #picker></mat-datepicker>\n              </mat-form-field><br>\n              <mat-divider class=\"divider\"></mat-divider>\n\n              <mat-form-field>\n                  <input type=\"number\" matInput placeholder=\"Payments-Enter Credit card\" formControlName=\"credit\" >\n                 \n              </mat-form-field><br>\n              <mat-divider class=\"divider\"></mat-divider>\n              </form>\n    </div>\n    <div class=\"msgerr\">\n        <h3 class=\"errmsg\" *ngIf=\"errMsg\" >Please check your form</h3>\n    </div>\n    \n    <mat-checkbox class=\"example-margin\" [(ngModel)]=\"checked\">Download invoice </mat-checkbox>\n    <mat-divider class=\"divider\"></mat-divider>\n    <button mat-flat-button class=\"submitbtn\" (click)=\"submitOrder()\" >Submit Order</button>\n    <button mat-flat-button color=\"primary\"  [mat-dialog-close]=\"true\">Back</button>\n    <div>\n        \n        \n    </div>\n    \n   \n  </div>\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/order/order.component.ts":
/*!******************************************!*\
  !*** ./src/app/order/order.component.ts ***!
  \******************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.min.js");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jspdf__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { DataSource } from '@angular/cdk/table';


var OrderComponent = /** @class */ (function () {
    function OrderComponent(dataService, router, dialog, dialogRef) {
        this.dataService = dataService;
        this.router = router;
        this.dialog = dialog;
        this.dialogRef = dialogRef;
        this.cart = [];
        this.errMsg = false;
        this.checked = false;
        this.order = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            street: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            date: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            credit: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
        });
    }
    OrderComponent.prototype.getUserData = function () {
        this.order.controls["city"].setValue(this.userData[0].city);
        this.order.controls["street"].setValue(this.userData[0].street);
    };
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.check()
            .subscribe(function (data) {
            if (data == "not loged") {
                console.log(data);
                _this.router.navigate(['/login']);
            }
            else {
                _this.userCart = data;
                _this.dataService.getCart(_this.userCart)
                    .subscribe(function (data) {
                    _this.cart = data;
                    _this.dataService.getSumOfOrder(_this.userCart)
                        .subscribe(function (data) {
                        _this.sumOfOrder = data;
                    });
                });
                _this.dataService.getUserData(_this.userCart)
                    .subscribe(function (data) {
                    _this.userData = data;
                });
            }
        });
    };
    OrderComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    OrderComponent.prototype.submitOrder = function () {
        var _this = this;
        var time = new Date();
        if (this.order.valid && isNaN(this.order.controls.credit.value) == false) {
            var ordrToSend = {
                "userId": this.userCart,
                "totalPrice": this.sumOfOrder,
                "city": this.order.controls.city.value,
                "street": this.order.controls.street.value,
                "dateSending": this.order.controls.date.value,
                "dateOrder": time,
                "digitCredit": this.order.controls.credit.value
            };
            if (this.checked) {
                // console.log(this.cart);
                // console.log(this.userCart);
                //sumOfOrder
                var temp = void 0;
                temp = this.cart;
                console.log(temp);
                var doc = new jspdf__WEBPACK_IMPORTED_MODULE_3__();
                doc.setFont('courier');
                doc.setFontType('normal');
                doc.setFontSize(24);
                // +'  , Your total sum :'+this.sumOfOrder 
                doc.text(10, 30, 'Hello ' + this.userCart);
                doc.setLineWidth(0.1);
                doc.line(10, 32, 185, 32);
                doc.setFontSize(15);
                doc.text(20, 40, "Item");
                doc.text(70, 40, "Amount");
                doc.text(120, 40, "Total price");
                doc.setLineWidth(1.5);
                doc.line(20, 42, 160, 42);
                var x = 50;
                doc.setFontSize(16);
                for (var i = 0; i < temp.length; i++) {
                    //doc.text(10,x, temp[i].productName +"   "+temp[i].amount+" "+temp[i].totalPrice);
                    doc.text(20, x, " " + temp[i].productName);
                    doc.text(70, x, " " + temp[i].amount);
                    doc.text(120, x, " " + temp[i].totalPrice);
                    // doc.setLineWidth(0.2);
                    // doc.line(20,(x+5),150,(x+5));
                    x += 5;
                }
                doc.setLineWidth(0.5);
                doc.line(20, (x + 2), 160, (x + 2));
                doc.setFontSize(24);
                doc.text(20, x + 10, 'Total Cart :        ' + this.sumOfOrder);
                doc.save("invoice.pdf");
                // this.dataService.invoice(temp)
                // .subscribe(data=>{
                //  })
            }
            this.dataService.submitOrder(ordrToSend)
                .subscribe(function (data) {
                _this.close();
            });
        }
        else {
            this.errMsg = true;
            setTimeout(function () { _this.errMsg = false; }, 2000);
        }
    };
    OrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order',
            template: __webpack_require__(/*! ./order.component.html */ "./src/app/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/order/order.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/product-detail/product-detail.component.css":
/*!*************************************************************!*\
  !*** ./src/app/product-detail/product-detail.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body{\r\n\tfont-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n    text-shadow: 4px 4px 4px #aaa;\r\n\t\tbackground-color:#92a7a3;\r\n\r\n}\r\n\r\n.alldata{\r\n\twidth:80%;\r\n}"

/***/ }),

/***/ "./src/app/product-detail/product-detail.component.html":
/*!**************************************************************!*\
  !*** ./src/app/product-detail/product-detail.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<body>\n    <div class=\"container-fluid \">\n        <div class=\"alldata\">\n            <h2 class=\"name\">{{name}}</h2>\n            <img class=\"img-responsive\" src=\"/assets/{{pic}}\">\n        \n            <h5 class=\"itemPname\">{{price | currency:'ILS':'symbol':'1.2-2'}}</h5>\n        \n      \n        </div>\n     \n      \n      </div>\n      <br>\n      \n      <button mat-raised-button [mat-dialog-close]=\"true\">Close</button>\n\n</body>\n\n"

/***/ }),

/***/ "./src/app/product-detail/product-detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/product-detail/product-detail.component.ts ***!
  \************************************************************/
/*! exports provided: ProductDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailComponent", function() { return ProductDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ProductDetailComponent = /** @class */ (function () {
    function ProductDetailComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.pic = this.data.pic;
        this.name = this.data.name;
        this.price = this.data.price;
    }
    ProductDetailComponent.prototype.ngOnInit = function () {
        console.log(this.data);
    };
    ProductDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-detail',
            template: __webpack_require__(/*! ./product-detail.component.html */ "./src/app/product-detail/product-detail.component.html"),
            styles: [__webpack_require__(/*! ./product-detail.component.css */ "./src/app/product-detail/product-detail.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ProductDetailComponent);
    return ProductDetailComponent;
}());



/***/ }),

/***/ "./src/app/registeration/registeration.component.css":
/*!***********************************************************!*\
  !*** ./src/app/registeration/registeration.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    background-color: black;\r\n}\r\n\r\n* {\r\n    box-sizing: border-box;\r\n}\r\n\r\n/* Add padding to containers */\r\n\r\n.container {\r\n    padding: 16px;\r\n    background-color:#f6d7d794;\r\n}\r\n\r\n/* Full-width input fields */\r\n\r\ninput[type=text], input[type=password]  {\r\n    width: 100%;\r\n    padding: 15px;\r\n    margin: 5px 0 22px 0;\r\n    display: inline-block;\r\n    border: none;\r\n    background: #f1f1f1;\r\n}\r\n\r\ninput[type=text]:focus, input[type=password]:focus  {\r\n    background-color: #ddd;\r\n    outline: none;\r\n}\r\n\r\n/* Overwrite default styles of hr */\r\n\r\nhr {\r\n    border: 1px solid #f1f1f1;\r\n    margin-bottom: 25px;\r\n}\r\n\r\n/* Set a style for the submit button */\r\n\r\n.registerbtn {\r\n    background-color: #7D88B3;\r\n    color: white;\r\n    padding: 16px 20px;\r\n    margin: 8px 0;\r\n    border: none;\r\n    cursor: pointer;\r\n    width: 40%;\r\n    opacity: 0.9;\r\n    \r\n}\r\n\r\n.registerbtn:hover {\r\n    /* opacity: 1; */\r\n    background-color: rgb(64, 84, 160);\r\n}\r\n\r\n/* Add a blue text color to links */\r\n\r\na {\r\n    color: dodgerblue;\r\n}\r\n\r\n/* Set a grey background color and center the text of the \"sign in\" section */\r\n\r\n.signin {\r\n    background-color: #f1f1f1;\r\n    text-align: center;\r\n}\r\n\r\n.errmsg{\r\n    background-color: rgb(255, 4, 4)\r\n}"

/***/ }),

/***/ "./src/app/registeration/registeration.component.html":
/*!************************************************************!*\
  !*** ./src/app/registeration/registeration.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "   <div class=\"container\">\n        \n    <span class=\"errmsg\" [hidden]=\"!errMsg\" [(ngModel)]=\"errtype\" ngDefaultControl> {{errtype}}</span>\n      \n            <form [formGroup]=\"registerForm\">\n                <h3>Register</h3>\n                <p>Please fill in this form to create an account.</p>\n        <div class=\"col-sm-12 col-md-4\">\n            <label for=\"idNumber\"><b>Id number</b></label>\n            <input formControlName=\"id\"  class=\"form-control\" type=\"text\" placeholder=\"Enter Id number\" name=\"idNumber\" >\n\n            <label for=\"email\"><b>Email</b></label>\n            <input formControlName=\"email\" type=\"email\"  class=\"form-control\" id=\"email\" placeholder=\"Enter Email\"  >\n\n            <label for=\"psw\"><b>Password</b></label>\n            <input formControlName=\"password\" type=\"password\"  id=\"psw\" class=\"form-control\" placeholder=\"Enter Password\" >\n            \n\n            <label for=\"pswrepeat\"><b>Confirm Password</b></label>\n            <input formControlName=\"pswrepeat\" type=\"password\"  class=\"form-control\" placeholder=\"Repeat Password\" id=\"pswrepeat\" >\n            \n            <button  class=\"form-control registerbtn advance\" (click)=\"check()\" >Advance</button>\n\n          \n                  \n                  <p>Already have an account? <a routerLink=\"/login\">Sign in</a></p>\n   \n        </div>\n        <div class=\"col-sm-12 col-md-1\"></div>\n        \n        <div class=\"col-sm-12 col-md-4\" *ngIf=\"isSectionFull\">\n            <h3 *ngIf=\"isSectionFull\">Please compleat the registration</h3>\n           \n        <label for=\"city\"><b>City</b></label>\n        <input formControlName=\"city\" class=\"form-control\" type=\"text\" placeholder=\"Enter City\" id=\"city\" >\n\n        <label for=\"street\"><b>Street</b></label>\n        <input formControlName=\"street\" class=\"form-control\" type=\"text\" placeholder=\"Enter street \" id=\"street\" >\n    \n        <label for=\"username\"><b>First name</b></label>\n        <input formControlName=\"username\" class=\"form-control\" type=\"text\" placeholder=\"Enter username\"id=\"username\">\n    \n        <label for=\"lastName\"><b>Last name</b></label>\n        <input formControlName=\"lastName\" class=\"form-control\" type=\"text\" placeholder=\"Enter last name\" id=\"lastName\" >\n               \n        <button type=\"button\" class=\"registerbtn register\" (click)=\"register()\">Register</button>\n       </div>\n\n    </form>\n    \n  </div>\n  \n  \n  \n    "

/***/ }),

/***/ "./src/app/registeration/registeration.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/registeration/registeration.component.ts ***!
  \**********************************************************/
/*! exports provided: RegisterationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterationComponent", function() { return RegisterationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import {FormBuilder, FormGroup, Validators} from '@angular/forms';


var RegisterationComponent = /** @class */ (function () {
    function RegisterationComponent(dataService) {
        this.dataService = dataService;
        this.isSectionFull = false;
        this.errMsg = false;
        this.errtype = "";
        this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            street: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            pswrepeat: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
        });
    }
    RegisterationComponent.prototype.ngOnInit = function () {
    };
    RegisterationComponent.prototype.check = function () {
        if (this.registerForm.controls.password.value != this.registerForm.controls.pswrepeat.value ||
            !this.registerForm.controls.id.value || !this.registerForm.controls.email.valid) {
            // this.errMsg="Please check your regestration details"
            this.errMsg = true;
            this.errtype = "Please check your regestration details";
        }
        else {
            this.isSectionFull = true;
            this.errMsg = false;
        }
    };
    RegisterationComponent.prototype.register = function () {
        var _this = this;
        if (!this.registerForm.valid || (this.registerForm.controls.password.value != this.registerForm.controls.pswrepeat.value)) {
            this.errMsg = true;
        }
        else {
            this.errMsg = false;
            this.dataService.register(this.registerForm.value)
                .subscribe(function (data) {
                if (data == "user already exist") {
                    _this.errMsg = true;
                    _this.errtype = data;
                }
            });
        }
    };
    RegisterationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-registeration',
            template: __webpack_require__(/*! ./registeration.component.html */ "./src/app/registeration/registeration.component.html"),
            styles: [__webpack_require__(/*! ./registeration.component.css */ "./src/app/registeration/registeration.component.css")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]])
    ], RegisterationComponent);
    return RegisterationComponent;
}());



/***/ }),

/***/ "./src/app/select/select.component.css":
/*!*********************************************!*\
  !*** ./src/app/select/select.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sel{\r\n\tborder:0.1px solid gray;\r\n\twidth:50px;\r\n\tmargin-bottom:3px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/select/select.component.html":
/*!**********************************************!*\
  !*** ./src/app/select/select.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-select  class=\"sel\" [(ngModel)]=\"amounttobuy\" (change)=\"update($event)\" >\n  <mat-option *ngFor=\"let num of amount\" [value]=\"num\" >{{num}}</mat-option>\n</mat-select>"

/***/ }),

/***/ "./src/app/select/select.component.ts":
/*!********************************************!*\
  !*** ./src/app/select/select.component.ts ***!
  \********************************************/
/*! exports provided: SelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectComponent", function() { return SelectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { EventEmitter } from 'protractor';
var SelectComponent = /** @class */ (function () {
    function SelectComponent() {
        this.amountToCart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.amount = [1, 2, 3, 4, 5, 6];
        this.amounttobuy = 1;
    }
    SelectComponent.prototype.ngOnInit = function () {
    };
    SelectComponent.prototype.update = function ($event) {
        console.log($event.value);
        this.amountToCart.emit($event.value);
        this.amounttobuy = 1;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], SelectComponent.prototype, "amountToCart", void 0);
    SelectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-select',
            template: __webpack_require__(/*! ./select.component.html */ "./src/app/select/select.component.html"),
            styles: [__webpack_require__(/*! ./select.component.css */ "./src/app/select/select.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SelectComponent);
    return SelectComponent;
}());



/***/ }),

/***/ "./src/app/shop-main/shop-main.component.css":
/*!***************************************************!*\
  !*** ./src/app/shop-main/shop-main.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shop-main/shop-main.component.html":
/*!****************************************************!*\
  !*** ./src/app/shop-main/shop-main.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  shop-main works!\n</p>\n"

/***/ }),

/***/ "./src/app/shop-main/shop-main.component.ts":
/*!**************************************************!*\
  !*** ./src/app/shop-main/shop-main.component.ts ***!
  \**************************************************/
/*! exports provided: ShopMainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopMainComponent", function() { return ShopMainComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShopMainComponent = /** @class */ (function () {
    function ShopMainComponent() {
    }
    ShopMainComponent.prototype.ngOnInit = function () {
    };
    ShopMainComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shop-main',
            template: __webpack_require__(/*! ./shop-main.component.html */ "./src/app/shop-main/shop-main.component.html"),
            styles: [__webpack_require__(/*! ./shop-main.component.css */ "./src/app/shop-main/shop-main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ShopMainComponent);
    return ShopMainComponent;
}());



/***/ }),

/***/ "./src/app/table/table.component.css":
/*!*******************************************!*\
  !*** ./src/app/table/table.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.allproducts{\r\n    border: 5px solid rgb(202, 212, 212);\r\n    background-color: azure;\r\n    border-radius: 5px;\r\n    \r\n \r\n    \r\n}\r\n.picitem{\r\n    width: 40px;\r\n    margin-left: 20%;\r\n    margin-top: 5px;\r\n   \r\n    \r\n}\r\n.product{\r\n\r\nborder: 1px solid brown;\r\nborder-radius: 5px;\r\nmargin: 5px;\r\nopacity: 0.8;\r\nheight: 170px;\r\ntext-align: center;\r\nwidth: 150px;\r\n\r\nalign-items: center;\r\n\r\n\r\n}\r\n.product:hover{\r\n    background-color: rgb(213, 226, 226);  \r\n   opacity: 1;\r\n\r\n}\r\n.addtocart{\r\n    margin-left: 5px;\r\n    border-radius: 5px;\r\n    background-color: rgb(117, 146, 207)\r\n}\r\n.categoryDiv{\r\n    width: 100%;\r\n    margin-bottom: 20px;\r\n\r\n}\r\n.divsearch{\r\n    height: 60px;\r\n    width: 40%;\r\nbackground-color: rgb(231, 231, 231);\r\n}\r\n.divsearch input{\r\n    float: right;\r\n    margin-right: 10px;\r\n}\r\n.divsearch span{\r\n    font-size: 18px;\r\n    margin-left: 5px;\r\n    font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/table/table.component.html":
/*!********************************************!*\
  !*** ./src/app/table/table.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "     \n    \n                <div class=\"container-fluid\">\n\n\n    <div class=\"cart col-sm-12 col-md-4 col-lg-4 \">\n        <app-cart-section [idEdit]=\"sendId\" [userCart]=\"sendUser\"></app-cart-section>\n       \n     </div>\n\n     <!-- {{currentUser}} -->\n\n     <span class=\"usermsg\" >wellcom :</span><span class=\"usermsg\">{{currentUser}}</span> \n\n\n\n\n      <div class=\"allproducts col-sm-6 col-md-6 col-lg-6\">\n            <div class=\"categoryDiv \">\n                    <nav class=\"navbar navbar-default\">\n                              <div class=\"navbar-header\">\n                                <strong><span>Select Category :</span></strong>\n                              </div><br> \n                      <ul class=\"nav navbar-nav\">\n                          <li *ngFor=\"let cat of categories\">\n                                <button mat-stroked-button color=\"primary\" [(ngModel)]=\"termCategory\" (click)=\"fillterCategory($event)\"  ngDefaultControl>{{cat.nameCategory}}</button>\n                          </li>\n                          <button mat-raised-button color=\"warn\" (click)=\"resetSearch()\">Reset</button>\n\n                      </ul>\n                             \n\n                          </nav>\n\n                          <div class=\"divsearch \" >\n                             <!-- <span>Search for product</span><input type=\"text\" (focus)=\"resetSearch()\" [(ngModel)]=\"term\"> -->\n\n                             <span>Search for product    </span><input matInput type=\"text\" placeholder=\"Search product\"  (focus)=\"resetSearch()\" [(ngModel)]=\"term\">\n\n                           </div>\n              \n            </div> <br>\n            <div class=\"product col-sm-5  col-md-3\"  *ngFor=\"let item of dat  | filter:term |filterCategory:termCategory  \">\n\n                    <!-- (click)=\"display($event)\" -->\n            <!-- <span [routerLink]=\"['/product']\">Detail</span> -->\n            <button mat-mini-fab color=\"primary\"  (click)=\"openDialog(item.picProduct,item.productName,item.price)\">Detail</button>\n              <img class=\"picitem\" src=\"/assets/{{item.picProduct}}\">\n              <h5 class=\"itemPname\">{{item.productName}}</h5>\n              <h5 class=\"itemPname\">{{item.price | currency:'ILS':'symbol':'1.2-2'}}</h5>\n\n                <app-select (amountToCart)=\"passAmount($event)\"></app-select>\n              \n                <button class=\"addtocart itemPname\" (click)=\"addButton(item)\">add to cart</button>\n                \n            </div>\n            \n      </div>\n\n\n\n\n\n\n\n\n\n      <div class=\"cartside col-sm-2 col-md-2 col-lg-2 \">\n          <!-- <app-product-detail></app-product-detail> -->\n        <marquee direction=\"up\" scrollamount= \"10\" height=\"400px\" scrollamount=\"7\" >\n          \n        <div class=\"product\" >\n            <img class=\"picitem\" src=\"/assets/{{randomPic.picProduct}}\">\n            <h4 class=\"itemPname\">{{randomPic.productName}}</h4>\n            <h4 class=\"itemPname\">{{randomPic.price | currency:'שח'}}</h4>\n             \n           \n        </div>\n           \n\n        </marquee>\n     </div>\n    </div>\n\n\n\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/table/table.component.ts":
/*!******************************************!*\
  !*** ./src/app/table/table.component.ts ***!
  \******************************************/
/*! exports provided: TableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableComponent", function() { return TableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../product-detail/product-detail.component */ "./src/app/product-detail/product-detail.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TableComponent = /** @class */ (function () {
    function TableComponent(dataService, dialog, router) {
        this.dataService = dataService;
        this.dialog = dialog;
        this.router = router;
        this.dat = "";
        this.categories = [];
        this.currentUser = "";
        this.randomPic = "";
        this.sendId = "";
        this.sendUser = "";
    }
    TableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.check()
            .subscribe(function (data) {
            if (data == "not loged") {
                console.log(data);
                _this.router.navigate(['/login']);
            }
            _this.sendUser = data;
            _this.currentUser = data;
            // console.log(data)
        });
        this.dataService.getCategories()
            .subscribe(function (data) {
            _this.categories = data;
        });
        this.dataService.allProducts()
            .subscribe(function (data) {
            console.log(data);
            _this.dat = data;
        });
        this.getRandomPic();
    };
    TableComponent.prototype.display = function (e) {
        e.preventDefault();
        alert("fghdfsgs");
    };
    TableComponent.prototype.openDialog = function (pic, name, price) {
        var _this = this;
        console.log(pic, name, price);
        this.dataService.check()
            .subscribe(function (data) {
            if (data == "not loged") {
                console.log(data);
                _this.router.navigate(['/login']);
            }
            else {
                _this.dialog.open(_product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_3__["ProductDetailComponent"], {
                    width: '40%',
                    height: "400px",
                    data: {
                        pic: pic,
                        name: name,
                        price: price
                    }
                });
                // let dialogRef = this.dialog.open(ProductDetailComponent, {
                // });
                // dialogRef.afterClosed().subscribe(result => {
                // });
            }
        });
    };
    TableComponent.prototype.getRandomPic = function () {
        var _this = this;
        setInterval(function () {
            _this.randomPic = _this.dat[Math.floor(Math.random() * _this.dat.length)];
        }, 5100);
    };
    TableComponent.prototype.fillterCategory = function (category) {
        this.termCategory = category.target.textContent;
    };
    TableComponent.prototype.resetSearch = function () {
        this.termCategory = "";
    };
    TableComponent.prototype.passAmount = function ($event) {
        console.log($event);
        this.amounttobuy = $event;
    };
    TableComponent.prototype.addButton = function (e) {
        var _this = this;
        if (this.amounttobuy == undefined) {
            e.amount = 1;
        }
        else {
            e.amount = this.amounttobuy;
        }
        e.userid = this.currentUser;
        // console.log(e.amount, e.userid,e)
        var assignValue = new Promise(function (resolve, reject) {
            if (e.amount != undefined) {
                resolve();
            }
            else {
                reject();
            }
        });
        assignValue.then(function () {
            // console.log(e)
            _this.dataService.makeCartItem(e);
            _this.sendId = e;
            _this.amounttobuy = 1;
            return e;
        }).catch(function () {
            console.log("value not set");
        });
    };
    TableComponent.prototype.setamount = function (event) {
        this.amounttobuy = event;
    };
    TableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-table',
            template: __webpack_require__(/*! ./table.component.html */ "./src/app/table/table.component.html"),
            styles: [__webpack_require__(/*! ./table.component.css */ "./src/app/table/table.component.css")],
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], TableComponent);
    return TableComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/pavel/shop/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map