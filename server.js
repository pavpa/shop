
const express   = require('express');
const bodyParser= require('body-parser');
const path =require('path');
const http = require('http');
const mongoose =require('mongoose');
const Schema=mongoose.Schema;
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
//require( path.resolve('./server/moduls/','passport-config.js') );


const app=express();
//login modul
require('./server/moduls/passport-config');

// API file for interacting with MongoDB
mongoose.connect("mongodb://localhost:27017/shopnew");

//  const cors = require('cors');
//  app.use(cors({
//     origin:['http://localhost:3000','http://127.0.0.1:3000'],
//     credentials:true

//  }))


app.use(passport.initialize());
app.use(passport.session());
app.use(session({
    secret:"yabadabado",
    resave: true,
    saveUninitialized: true,
    name: 'mystoreapp',
    cookie: {
      httpOnly: false,
      maxAge: 2000 * 60 * 5
    },
  store: new MongoStore({
          url: 'mongodb://localhost:27017/shopnew',
          ttl: 2*24*60
    })
}));

const router = express.Router();


  


 const users =require('./server/routes/users');
 //const login =require('./server/routes/login');
 const api =require('./server/routes/api');
 const files =require('./server/routes/files');

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// API location
//app.use('/login', login);
app.use('/users', users);
app.use('/api', api);
app.use('/files', files);



// Send all other requests to the Angular app
app.use(express.static('dist'));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});



const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));



