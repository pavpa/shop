import { Component, OnInit, Output, EventEmitter } from '@angular/core';
// import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  @Output() amountToCart: EventEmitter<number> =
             new EventEmitter<number>();
  amount: any = [1, 2, 3, 4, 5, 6];
   amounttobuy: any = 1;
  constructor() { }

  ngOnInit() {
  }
  update($event) {
    console.log($event.value);
     this.amountToCart.emit($event.value);
      this.amounttobuy = 1;
  }

}
