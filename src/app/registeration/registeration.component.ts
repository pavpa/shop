import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
// import {FormBuilder, FormGroup, Validators} from '@angular/forms';



import { DataService} from '../data.service';
import 'rxjs/Rx';


@Component({
  selector: 'app-registeration',
  templateUrl: './registeration.component.html',
  styleUrls: ['./registeration.component.css']
})
export class RegisterationComponent implements OnInit {

  constructor(private  dataService:  DataService) { }
  isSectionFull = false;
  errMsg = false;
  errtype: any = '';

    ngOnInit() {
    }
  registerForm: FormGroup = new FormGroup({
      id: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.email, Validators.required]),
      city: new FormControl(null, Validators.required),
      street: new FormControl(null, Validators.required),
      username: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      pswrepeat: new FormControl(null, Validators.required),
  });

   check() {
     if (this.registerForm.controls.password.value != this.registerForm.controls.pswrepeat.value ||
      !this.registerForm.controls.id.value || !this.registerForm.controls.email.valid) {
       // this.errMsg="Please check your regestration details"
       this.errMsg = true;
       this.errtype = 'Please check your regestration details';

      } else {
         this.isSectionFull = true;
         this.errMsg = false;
      }
   }

   register() {
     if (!this.registerForm.valid || (this.registerForm.controls.password.value != this.registerForm.controls.pswrepeat.value)) {
      this.errMsg = true;

     } else {
      this.errMsg = false;
      this.dataService.register(this.registerForm.value)
        .subscribe(data => {
          if (data == 'user already exist') {

            this.errMsg = true;
            this.errtype = data;
          }
        });
     }
   }




}


