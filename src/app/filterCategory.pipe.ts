import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/filter';


@Pipe({
  name: 'filterCategory'
})
export class FilterCategoryPipe implements PipeTransform {


  transform(dat: any, termCategory: any): any {
    if (termCategory === undefined ) { return dat; }
      return dat.filter((products) => {
              return   products.categoryId.includes(termCategory);
           });
  }

}











