import { Component, OnInit, Inject } from '@angular/core';
import {ActivatedRoute}  from '@angular/router';
import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';

import { DataService} from '../data.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }
  pic: any = this.data.pic;
  name: any = this.data.name;
  price: any = this.data.price;

  ngOnInit() {
    console.log( this.data);
  }

}
