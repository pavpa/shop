import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { element } from 'protractor';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { DataService} from '../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-edited-item',
  templateUrl: './edited-item.component.html',
  styleUrls: ['./edited-item.component.css']
})
export class EditedItemComponent implements OnInit {
  @Input() public idEdit: any;
  @Output () messageEvent = new EventEmitter<string>();
  updateView = 'updateView';

  constructor(private  dataService:  DataService, private router: Router) { }

  ngOnInit() {
  }

  update() {
    //  let update={id:this.idEdit._id , productName:this.idEdit.productName , categoryId:this.idEdit.categoryId , price:this.idEdit.price}
    console.log(this.idEdit);
     return this.dataService.editProduct(this.idEdit)
     .subscribe(data => {
       this.messageEvent.emit(this.updateView);
       this.idEdit = {};


    });
  }

}
