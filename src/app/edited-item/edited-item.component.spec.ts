import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditedItemComponent } from './edited-item.component';

describe('EditedItemComponent', () => {
  let component: EditedItemComponent;
  let fixture: ComponentFixture<EditedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
