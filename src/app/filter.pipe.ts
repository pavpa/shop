import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/filter';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(dat: any, term: any): any {
    if (term === undefined) { return dat; }
    return  dat.filter((product) => {
          return product.productName.toLowerCase().includes(term.toLowerCase());
     });

   }

}
