import { Component, OnInit } from '@angular/core';
import { DataService} from '../data.service';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';


const uri = '/files/upload/';
@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: uri});

  addItem: FormGroup = new FormGroup({
    Pname     : new FormControl(null, Validators.required),
    Pcategory : new FormControl(null, [Validators.required]),
    Price     : new FormControl(null, Validators.required),
    picPruduct: new FormControl(null, Validators.required)
 });


// isNaN('') not a number


  constructor(private  dataService:  DataService) { }


  ngOnInit() {
  }

  addProduct() {
    this.dataService.addProduct(this.addItem.value);
    this.uploader.uploadAll();
    location.reload();

  }


}
