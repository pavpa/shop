import { Component, OnInit, Input } from '@angular/core';
import { DataService} from '../data.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { OrderComponent} from '../order/order.component';
import {Router}   from '@angular/router';


@Component({
  selector: 'app-cart-section',
  templateUrl: './cart-section.component.html',
  styleUrls: ['./cart-section.component.css']
})
export class CartSectionComponent implements OnInit {
  @Input() public idEdit: any;
  @Input() public userCart: any;
  cart: any = [];
  confirmEmptyCart: Boolean = false;

  constructor(private  dataService:  DataService, public dialog: MatDialog, private router: Router) { }
  userOrder: any;


  ngOnInit() {

  }


  ngOnChanges() {
   this.dataService.getCart(this.userCart)
   .subscribe(data => {
     this.cart = data;

     this.userOrder = this.userCart;
   });
    this.cart.push(this.idEdit);
  }

  applyDel(e) {
    this.confirmEmptyCart = true;
    e.target.textContent = 'Cancel';
    console.log(e);
  }


  openDialog(): void {
    this.dataService.check()
    .subscribe(data => {
      if (data == 'not loged') {
        console.log(data);
        this.router.navigate(['/login']);
      } else {
        const dialogRef = this.dialog.open(OrderComponent, {
          width: '80%',
          height: '600px',
        });
        dialogRef.afterClosed().subscribe(result => {
          this.cart = [];
        });
      }
    });
  }

  removeitem(id) {
    this.dataService.removeItemFromCart(id)
    .subscribe(data => {
        this.dataService.getCart(this.userCart)
          .subscribe(data => {
            this.cart = data;
          });

    });
  }
  cancelDeleteAll() {
    document.getElementById('remove').innerHTML = 'Remove All Products';
    this.confirmEmptyCart = false;
  }

  removeAll(user) {
    this.dataService.removeAllFromCart(user)
    .subscribe(data => {
      this.dataService.getCart(this.userCart)
          .subscribe(data => {
            document.getElementById('remove').innerHTML = 'Remove All Products';
            this.confirmEmptyCart = false;
            this.cart = data;
          });
    });
  }
}
