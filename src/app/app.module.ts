import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CanActivate}  from '@angular/router';

import { HttpModule } from '@angular/http';
import { HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';

import { DataService } from './data.service';
import { HeaderComponent } from './header/header.component';
import { LogInPageComponent } from './log-in-page/log-in-page.component';
import { RegisterationComponent } from './registeration/registeration.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NoopAnimationsModule} from '@angular/platform-browser/animations';

import { ShopMainComponent } from './shop-main/shop-main.component';
import { CartSectionComponent } from './cart-section/cart-section.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { ProtectGauardService } from './admin-page/protect-gauard.service';


import { RouterModule, Routes} from '@angular/router';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { EditedItemComponent } from './edited-item/edited-item.component';
import { FilterPipe } from './filter.pipe';
import { AddItemComponent } from './add-item/add-item.component';
import { FilterCategoryPipe } from './filterCategory.pipe';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {OrderComponent} from './order/order.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { SelectComponent } from './select/select.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';





const appRoutes: Routes = [
   {path: 'register', component: RegisterationComponent},
   {path: 'login', component: LogInPageComponent},
   {path: 'product/:id', component: ProductDetailComponent},

   {path: '', component: LogInPageComponent},
   {path: 'admin',
           canActivate: [ProtectGauardService],
               component: AdminPageComponent},
   {path: 'add', component: AddItemComponent},
   {path: 'shopmain', component: TableComponent},
   {path: 'order', component: OrderComponent}

];

@NgModule({
  declarations: [


    AppComponent,
    TableComponent,
    HeaderComponent,
    LogInPageComponent,
    RegisterationComponent,
    ShopMainComponent,
    CartSectionComponent,
    AdminPageComponent,
    EditedItemComponent,
    AddItemComponent,
    FilterPipe,
    FilterCategoryPipe ,
    FileSelectDirective,
    OrderComponent,
    SelectComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCheckboxModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatButtonModule,
    MatDialogModule,
   MatSelectModule,
    MatDialogModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDialogModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [

    OrderComponent
  ],

  providers: [DataService, ProtectGauardService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
