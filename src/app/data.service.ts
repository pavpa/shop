import { Injectable } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import {Router} from '@angular/router';

//import { all } from 'q';

@Injectable()
export class DataService {

constructor(private http: HttpClient, private router: Router ) { }


 userToComponent: any = '';


addProduct(newProduct) {
  console.log(newProduct);
  this.http.post(`/api/createProduct`, {newProduct})
  .subscribe(
    (data: any) => {
    }
  );
}
invoice(dat) {
 return  this.http.post('/files/creatInvoice', {dat} )
  .map(
    (data: any) => {

      return data;
    }
  );
}


 editProduct(newProduct) {
  return this.http.put(`/api/editProduct`, newProduct)
  .map(
    (data: any) => {
      return(data);
      // console.log(data)
    }
  );

 }


check() {
  return this.http.get(`/users/auth`)
  .map(
    (data: any) => {

      return data;
    }
  );
}

getUserData(username) {

  return this.http.get(`/users/getUserData/` + username, {responseType: 'json'})
  .map(
    (data: any) => {

      return data;
    }
  );
 }





 allProducts() {
  return this.http.get(`/api/getAllProducts`)
  .map(
    (data: any) => {
      return data;
    }
  );
 }

getCategories() {
  return this.http.get(`/api/getCategories`)
  .map(
    (data: any) => {
      return data;
    }
  );
}

getNumOfProducts() {
  return this.http.get(`/api/numAllProducts`)
  .map(
    (data: any) => {
      return data;
    }
  );
}

getNumOfOrders() {
  return this.http.get(`/api/numorders`)
  .map(
    (data: any) => {
      return data;
    }
  );
}

getCart(name) {

  return this.http.get('/api/getCurrentCart/' + name , {responseType: 'json'})
  .map(
          (data: any) => {

              return data;
           });
}

removeItemFromCart(id) {
  const url: any = '/api/removeItemFromCart/' + id;
  return this.http.delete(url , {responseType: 'text'})
  .map(
          (response: any) => {
              return response;
           });
}

removeAllFromCart(user) {
  const url: any = '/api/removeAllFromCart/' + user;
  return this.http.delete(url , {responseType: 'text'})
  .map(
          (response: any) => {
              return response;
           });
}
checkIfCart(name) {
  const url: any = '/api/checkifcart/' + name;
  return this.http.get(url , {responseType: 'text'})
  .map(
          (response: any) => {
            return response;
           });
 }

checkIfOrder(name) {
  const url: any = '/api/checkifOrder/' + name;
  return this.http.get(url , {responseType: 'text'})
  .map(
          (response: any) => {
              return response;
           });
        } //

getcartItems(name) {
  const url: any = '/api/getcartItems/' + name;
  return this.http.get(url , {responseType: 'text'})
  .map(
          (response: any) => {
              return response;
            });
        } //
makeCart(newCart) {
 this.http.post(`/api/addCart`, {name: newCart})
  .subscribe(
    (data: any) => {
    }
  );
}
makeCartItem(newCartItem) {
 this.http.post(`/api/addItemToCart`, {newCartItem})
  .subscribe(
    (data: any) => {
    }
  );
}

register(newuser) {
  return this.http.post(`users/register`, newuser)
  .map(
    (data: any) => {
      if (data.msg == 'user already exist') {
        return data.msg;
      } else {
           this.router.navigate(['/login']);
          }
    }
  );
}
login(currenuser) {

  return this.http.post(`/users/login`, currenuser , {responseType: 'text'})
  .map(
         (response: any) => {


         return { response};

         });
        }
  logOut() {

    return this.http.get(`/users/logout` , {responseType: 'text'})
    .map(
           (response: any) => {
            this.router.navigate(['/login']);

           });
          }

getSumOfOrder(name) {
  return this.http.get('/api/getsumoforder/' + name , {responseType: 'json'})
  .map(
          (data: any) => {

              return data;
           });
}

submitOrder(order) {


  return this.http.post(`/api/makeorder`, order, {responseType: 'json'})
  .map(data => {
    return data;

  });
}







}

