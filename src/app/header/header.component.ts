import { Component, OnInit } from '@angular/core';
// import { LogInPageComponent } from '../log-in-page/log-in-page.component';
import { DataService} from '../data.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private dataService: DataService) { }

  logo = '../assets/header/mainBarImage.png';

  ngOnInit() {

  }

  logof() {
    this.dataService.logOut()
    .subscribe(data => {


    });
  }

}
