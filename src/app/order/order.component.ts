import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import * as jsPDF from 'jspdf';

// import { DataSource } from '@angular/cdk/table';
import { DataService} from '../data.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  cart: any = [];
  userCart: any;
  userData: any;
  city: any;
  sumOfOrder: any;
  errMsg = false;
  checked = false;




  constructor(private  dataService:  DataService, private router: Router, public dialog: MatDialog, public dialogRef: MatDialogRef<OrderComponent>) { }

  order: FormGroup = new FormGroup({
    city: new FormControl(null, Validators.required),
    street: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required),
    credit: new FormControl(null, [Validators.required]),
  });
  getUserData() {
      this.order.controls['city'].setValue(this.userData[0].city  );
      this.order.controls['street'].setValue(this.userData[0].street  );

  }

  ngOnInit() {
  this.dataService.check()
  .subscribe(data => {

    if (data == 'not loged') {
      console.log(data);
      this.router.navigate(['/login']);
    } else {
      this.userCart = data;
          this.dataService.getCart(this.userCart)
        .subscribe(data => {
          this.cart = data;
          this.dataService.getSumOfOrder(this.userCart)
            .subscribe(data => {
              this.sumOfOrder = data;

            });
        });
        this.dataService.getUserData(this.userCart)
        .subscribe(data => {
          this.userData = data;

        });
    }
   });
  }
  close(): void {
    this.dialogRef.close();
  }
  submitOrder() {
    const time = new Date();


    if (this.order.valid && isNaN(this.order.controls.credit.value) == false ) {
     const ordrToSend = {
         'userId'     : this.userCart,
         'totalPrice' : this.sumOfOrder,
         'city'       : this.order.controls.city.value,
         'street'     : this.order.controls.street.value,
         'dateSending': this.order.controls.date.value,
         'dateOrder'  : time,
         'digitCredit': this.order.controls.credit.value
    };

    if (this.checked) {

      // console.log(this.cart);
      // console.log(this.userCart);
      //sumOfOrder
      let temp;
      temp = this.cart;
      console.log(temp);
      const doc = new jsPDF();




      doc.setFont('courier');
      doc.setFontType('normal');
      doc.setFontSize(24);
      // +'  , Your total sum :'+this.sumOfOrder
      doc.text(10, 30, 'Hello ' + this.userCart );
      doc.setLineWidth(0.1);
      doc.line(10, 32, 185, 32);
      doc.setFontSize(15);
      doc.text(20, 40, 'Item');
      doc.text(70, 40, 'Amount');
      doc.text(120, 40, 'Total price');
      doc.setLineWidth(1.5);
      doc.line(20, 42, 160, 42);
      let x = 50;
        doc.setFontSize(16);
      for ( let i = 0; i < temp.length ; i++ ) {
        //doc.text(10,x, temp[i].productName +"   "+temp[i].amount+" "+temp[i].totalPrice);
        doc.text(20, x, ' ' +  temp[i].productName);
        doc.text(70, x, ' ' +   temp[i].amount);
        doc.text(120, x, ' ' +  temp[i].totalPrice);
        // doc.setLineWidth(0.2);
        // doc.line(20,(x+5),150,(x+5));

        x += 5;
      }
      doc.setLineWidth(0.5);
      doc.line(20, (x + 2), 160, (x + 2));
      doc.setFontSize(24);
      doc.text(20, x + 10,       'Total Cart :        ' + this.sumOfOrder );
       doc.save('invoice.pdf');

    // this.dataService.invoice(temp)
    // .subscribe(data=>{


    //  })



    }

    this.dataService.submitOrder(ordrToSend)
      .subscribe(data => {
             this.close();
      });
    } else {
      this.errMsg = true;
      setTimeout(() => {this.errMsg = false; }, 2000);
    }
  }
}
