import { Component, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import {FilterPipe} from '../filter.pipe';
 import {FilterCategoryPipe} from '../filterCategory.pipe';
 import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';

import { DataService} from '../data.service';
import { ProductDetailComponent} from '../product-detail/product-detail.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],


})
export class TableComponent implements OnInit {

  constructor(private  dataService:  DataService, public dialog: MatDialog, private router: Router) { }

 dat: any = '';
 categories: any = [];
 term: any;
 termCategory: any;
 amounttobuy: number;
 currentUser: any = '';
 randomPic: any = '';
 public sendId = '';
 public sendUser: any = '';



ngOnInit() {
this.dataService.check()
  .subscribe(data => {

    if (data == 'not loged') {
      console.log(data);
      this.router.navigate(['/login']);
    }
    this.sendUser = data;
    this.currentUser = data;
    // console.log(data)

  });
  this.dataService.getCategories()
  .subscribe(data => {
    this.categories = data;
  });
  this.dataService.allProducts()
  .subscribe(data => {
    console.log(data);
     this.dat = data;


  });

  this.getRandomPic();
}
display(e) {
  e.preventDefault();
  alert('fghdfsgs');
}



openDialog(pic, name, price): void {


  console.log(pic, name, price);
  this.dataService.check()
  .subscribe(data => {
    if (data == 'not loged') {
      console.log(data);
      this.router.navigate(['/login']);
    } else {


   this.dialog.open(ProductDetailComponent, {
      width: '40%',
      height: '400px',
      data: {
        pic: pic,
        name: name,
        price: price
      }


   } );
      // let dialogRef = this.dialog.open(ProductDetailComponent, {


      // });
      // dialogRef.afterClosed().subscribe(result => {
      // });
    }
  });
}


 getRandomPic() {
  setInterval(() => {

   this.randomPic = this.dat[Math.floor(Math.random() * this.dat.length)];

  }, 5100);
}

fillterCategory(category) {
  this.termCategory = category.target.textContent;

}
resetSearch() {
  this.termCategory = '';
}

passAmount($event) {
  console.log($event);
  this.amounttobuy = $event;
}
  addButton(e) {


    if (this.amounttobuy == undefined) {
      e.amount = 1;
    } else {
      e.amount = this.amounttobuy;

    }
    e.userid = this.currentUser;
// console.log(e.amount, e.userid,e)
    const assignValue = new Promise((resolve, reject) => {

      if (e.amount != undefined  ) {
        resolve();
      } else {
        reject();
      }
    });
    assignValue.then(() => {
      // console.log(e)
      this.dataService.makeCartItem(e);
      this.sendId = e;
      this.amounttobuy = 1;
      return e;
    }).catch(() => {
      console.log('value not set');
    });


  }

  setamount(event) {
       this.amounttobuy = event;
  }



}
