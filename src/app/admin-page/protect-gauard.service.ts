import { Injectable } from '@angular/core';
import {CanActivate}  from '@angular/router';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';



@Injectable({
  providedIn: 'root'
})
export class ProtectGauardService  implements CanActivate {
check: any;
  constructor(private http: HttpClient, private router: Router) { }



  canActivate(): boolean  {
    this.check = this.http.get('/users/protectadmin');
      if (this.check == false) {
        this.router.navigate(['/']);
      }
      return this.check;
  }
}
