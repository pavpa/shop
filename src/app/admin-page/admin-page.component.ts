import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {EditedItemComponent} from '../edited-item/edited-item.component';

import { DataService} from '../data.service';

import {Router} from '@angular/router';
import { Pipe, PipeTransform } from '@angular/core';
import {FilterPipe } from  '../filter.pipe';
import {FilterCategoryPipe} from '../filterCategory.pipe';
@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']


})
export class AdminPageComponent implements OnInit {

  constructor(private  dataService:  DataService, private router: Router) { }
  tuggelEdit = false;
  tuggelAdd = false;
  dat: any = [];
  term: any;
  public sendId = '';
  categories: any = [];
  currentUser: any = '';
  termCategory: any;

  receive($event) {
    console.log($event);
    if ($event == 'updateView') {
      this.categories = [];
      this.dat = [];
      return this.dataService.getCategories()
      .subscribe(data => {
        this.categories = data;
        this.dataService.allProducts()
        .subscribe(data => {
           this.dat = data;

        });
      });


    }
  }

  ngOnInit() {
    this.dataService.check()
  .subscribe(data => {
    if (data == 'not loged') {
      // console.log(data)
      this.router.navigate(['/login']);
    }

    this.currentUser = data;
    // console.log(data)

  });
  this.dataService.getCategories()
  .subscribe(data => {
    this.categories = data;
  });
  this.dataService.allProducts()
  .subscribe(data => {
     this.dat = data;

  });


  }

  fillterCategory(category) {
    this.termCategory = category.target.textContent;

  }
  resetSearch() {
    this.termCategory = '';
  }
  filterBy(e) {

  }


  editme(e) {
    this.dat = [];
    this.dataService.allProducts()
  .subscribe(data => {
     this.dat = data;

  });
    this.sendId = e;
    this.tuggelAdd = false;
    this.tuggelEdit = true;
   return e;
  }

  tuggelAddBtn() {
    this.tuggelEdit = false;
    this.tuggelAdd = true;

  }




}
