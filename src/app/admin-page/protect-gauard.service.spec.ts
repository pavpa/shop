import { TestBed, inject } from '@angular/core/testing';

import { ProtectGauardService } from './protect-gauard.service';

describe('ProtectGauardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProtectGauardService]
    });
  });

  it('should be created', inject([ProtectGauardService], (service: ProtectGauardService) => {
    expect(service).toBeTruthy();
  }));
});
