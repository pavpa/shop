import { Component, OnInit } from '@angular/core';
import {Router}   from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import { DataService} from '../data.service';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';

@Component({
  selector: 'app-log-in-page',
  templateUrl: './log-in-page.component.html',
  styleUrls: ['./log-in-page.component.css']
})
export class LogInPageComponent implements OnInit {

    check: any = '';
    userdata: any = '';
    continueStyle = '#fff';
    buttonStatus = true;
    currentClass: any = 'continueShoppingDiabled loginmodal-submit';
    numProducts: any = '';
    numOrders: any = '';
    MsgAfterLog: any = '';
    sumCurentCart: any = 0;
    dateFoundCart: any = '';

    loginForm: FormGroup = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password: new FormControl(null, Validators.required)
  });

  constructor(private router: Router, private dataService: DataService) { }



  ngOnInit() {
   this.dataService.getNumOfProducts()
   .subscribe(data => {
     this.numProducts = data;

   });

    this.dataService.getNumOfOrders()
    .subscribe(data => {
      this.numOrders = data;
    });
  }

  login() {
    this.dataService.login(this.loginForm.value)
    .subscribe(
      data => {
        if (JSON.parse(data.response) == 'invalid user/password') {

        this.MsgAfterLog = JSON.parse(data.response);
             console.log(JSON.parse(data.response));
        } else {
          this.userdata = JSON.parse(data.response);
          if (this.userdata.roll == 1) {
            this.router.navigate(['/admin']);
          } else {
                //check if active cart
                this.dataService.checkIfCart(this.loginForm.controls.username.value)
                 .subscribe(data => {
                  data = JSON.parse(data);
                    //case if  active cart
                  if (data.length != 0) {
                    this.dateFoundCart = data[0].shoppingCarDate;
                    this.dateFoundCart = this.dateFoundCart.split(' ', 4).toString().replace(',', ' ');
                    //console.log(this.dateFoundCart)
                     this.dataService.getcartItems(this.loginForm.controls.username.value)
                      .subscribe(response => {
                        response = JSON.parse(response);
                        this.sumCurentCart = 0;
                          for (let i = 0; i < response.length; i++) {
                            this.sumCurentCart += response[i].totalPrice;
                          }
                        this.buttonStatus = false;
                        this.currentClass = 'continueShopping loginmodal-submit';

                        this.MsgAfterLog = 'welcome back ,you have a cart from  \n' + this.dateFoundCart + '  with the sum ' + Math.round(this.sumCurentCart * 100) / 100 + ' שח';
                      });
                  } else {
                    this.dataService.checkIfOrder(this.loginForm.controls.username.value)
                    .subscribe(orderdata => {
                      if (orderdata.length == 0 ) {
                        this.MsgAfterLog = 'Welcome  to your first  buy';
                        this.buttonStatus = false;
                        this.currentClass = 'continueShopping loginmodal-submit';
                      } else {
                        this.MsgAfterLog = 'Your last order was at , ' + JSON.parse(orderdata);
                        this.buttonStatus = false;
                        this.currentClass = 'continueShopping loginmodal-submit';

                      }
                    });
                  }
                });
              }


        }
     });
   }
   continue() {
    this.dataService.checkIfCart(this.loginForm.controls.username.value)
    .subscribe(data => {
      data = JSON.parse(data);
     // console.log(data.length,this.loginForm.controls.username.value)
      if (data.length == 0) {
            this.dataService.makeCart(this.loginForm.controls.username.value);
            this.router.navigate(['/shopmain']);
      } else {
        this.router.navigate(['/shopmain']);
      }
     });
   }

}
