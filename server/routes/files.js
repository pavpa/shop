const express = require('express');
const path =require('path');
const multer =require('multer');
const pathToUpload="./../assets/upload/";
const cartItem=require( path.resolve('./server/moduls/','cartItem.js') );
const dateFormat = require('dateformat');

const pdf=require('pdfkit');
const fs =require("fs-extra");


const router = express.Router();

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
       cb(null, './dist/assets/');

       
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.originalname);
    }
});

var upload = multer({ //multer settings
    storage: storage
}).single('file');



router.post("/upload",(req,res,next)=>{
    upload(req,res,function(err){
        if(err){
            return res.status(501).json({error:err})
        }
        return res.json({originalname:req.body.name,uploadname:req.body.name});
    })
})

//invoice
// router.get("/downloadinvoice",(req,res)=>{
//    let file = 'invoice.pdf';
//     res.sendFile(file);
// })




router.post("/creatInvoice",(req,res,next)=>{
    let dayBuy=new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '').substr(0, 10)

    let i=req.body.dat.length-1;
    let invoice= new pdf;
    let fileName = dayBuy+'.pdf'; 
   
    invoice.pipe(fs.createWriteStream(fileName));
    invoice.text(`hello , ${req.body.dat[0].cartId}\n`);
    invoice.text(`------------------\n`);
    req.body.dat.forEach((item)=>{
      invoice.text(`item : ${item.productName} , amount : ${item.amount} Total price :${item.totalPrice}`)


    })
    invoice.text(`------------------\n`);
    invoice.text(`Total Cart :${req.body.dat[i].sumOfOrder}\n`);
    invoice.end();
    // let filepath=path.join(__dirname,'../../')+'/'+dayBuy+'.pdf';
    // res.sendFile(filepath);
    // var filePath = "./"; 
    // var fileName = 'invoice.pdf'; 
    // res.download(filePath,fileName)
     res.json(fileName)     

})









module.exports = router;