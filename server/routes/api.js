const express = require('express');
const path =require('path');

const shoppingCart=require( path.resolve('./server/moduls/','shoppingCart.js') );
const productItem=require( path.resolve('./server/moduls/','productItem.js') );
const orderModule=require( path.resolve('./server/moduls/','orderModule.js') );
const categoryItem=require( path.resolve('./server/moduls/','categoryItem.js') );
const cartItem=require( path.resolve('./server/moduls/','cartItem.js') );


const router = express.Router();


 
router.post ('/createProduct/',categoryItem.checkCategory,productItem.initProduct,(req,res,next)=>{res.sendStatus(200)});
router.put  ('/editProduct',categoryItem.checkCategoryUpdate,productItem.updateProduct,(req,res,next)=>{});
router.delete  ('/removeItemFromCart/:id',cartItem.removeItemFromCart,(req,res,next)=>{});
router.delete  ('/removeAllFromCart/:user',cartItem.delAllFromCart,(req,res,next)=>{});
router.get  ('/getCurrentCart/:name',cartItem.getCart,(req,res,next)=>{});
router.get  ('/getAllProducts',productItem.getAllProducts,(req,res,next)=>{});
router.get  ('/getCategories',categoryItem.getAllCategories,(req,res,next)=>{});
router.post ('/addCart/',shoppingCart.makeCart,(req,res,next)=>{res.sendStatus(200)});
router.get  ('/getcartItems/:name',cartItem.getCart,(req,res,next)=>{});
router.get  ('/numAllProducts',productItem.numProducts,(req,res,next)=>{});
router.get  ('/numorders',orderModule.numorders,(req,res,next)=>{});
router.get  ('/checkifcart/:name',shoppingCart.findCart,(req,res,next)=>{}) ;
router.get  ('/checkifOrder/:name',orderModule.findOrder,(req,res,next)=>{});
router.post ('/addItemToCart/',cartItem.itemInCart,(req,res,next)=>{res.sendStatus(200)});


router.get  ('/getsumoforder/:name',cartItem.sumOfItemsCart,(req,res,next)=>{});

router.post ('/makeorder/',orderModule.allorders,cartItem.delAllAfterOrder,shoppingCart.delCartafteOrder,(req,res,next)=>{
    res.status(200).json("done");

})






module.exports = router;