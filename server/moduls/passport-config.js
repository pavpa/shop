
const express   = require('express');
const session = require('express-session'); 
const mongoose =require('mongoose');
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
  const path =require('path');
  const user=require( path.resolve('./server/moduls/','userModule.js') );
  const MongoStore = require('connect-mongo')(session);
  const bcrypt =require('bcrypt');
  const cookieSession=require('cookie-session');
  const app=express();

  

   
   

   
  passport.use(new LocalStrategy((username,password,done)=>{
    // validate username and Password
    user.findOne({username:username},(err,data)=>{
      let errMsg="invalid user name / password"
      if(err){
        
        return done(err)
      }
      if (!data){
        return done(null,false,{message:errMsg});
      }
      
      
      if (!bcrypt.compareSync(password,data.password)){
       return   done(null,false,{message:errMsg});
     }
  
      return done(null,data);
  
    })
  
  }))
  passport.serializeUser((user,done)=>{
    done(null,user._id);
  })
  passport.deserializeUser((user,done)=>{
   user.findOne({id:user._id},(err,user)=>{
     done(err,user);
    })
  
  })