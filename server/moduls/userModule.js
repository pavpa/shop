const mongoose =require('mongoose');
const Schema=mongoose.Schema;
const bcrypt =require('bcrypt');


//user Schema
const userSchema= new Schema({
    id:String,
    email:String,
    password:String,
    city:String,
    street:String,
    username:String,
    lastName:String,
    role:Number
   });


   userSchema.statics.hashPassword = function hashPassword(password){
     return bcrypt.hashSync(password ,10)
   }

   userSchema.methods.isvalid=function(hashedPassword){
     return bcrypt.compareSync(hashedPassword,this.password)
   }



   //const user=mongoose.model('users',userSchema);  

   const user=module.exports=mongoose.model('users',userSchema);  