const mongoose=require("mongoose");
const express = require('express');
const Schema=mongoose.Schema;
const dateFormat = require('dateformat');


const orderSchema= new Schema({
    userId     :String,
    
    totalPrice :Number,
    city       :String,
    street     :String,
    dateSending:Date,
    dateOrder  :Date,
    digitCredit:Number
  })
  const order=mongoose.model("orders",orderSchema);

  const allorders=(req,res,next)=>{
    const ord=new order({
      userId:req.body.userId,
      totalPrice:req.body.totalPrice,
      city:req.body.city,
      street:req.body.street,
      dateSending:req.body.dateSending,
      dateOrder:req.body.dateOrder,
      digitCredit:req.body.digitCredit
    })
    ord.save();
    return next();
  }
  
  const findOrder=(req,res,next)=>{
    order.findOne({"userId":req.params.name},(err,data)=>{
      if(data){
       
        res.json(dateFormat(data.dateOrder, "fullDate"));
      
      
      }else{
        res.json()
      }
  }
  )}

  const numorders=(req,res,next)=>{
    order.count({},(err,count)=>{
      if(err){
        return err
      }
       res.json(count)
    })
    return next();
  }

  const checkDate=(req,res,next)=>{
    order.count({"dateSending":req.params.date},(err,count)=>{
      if(err){
        return err
      }
      res.json(count);
      return next();
  
  
    })
  
  }
  const createOrderFile=(req,res,next)=>{

    cartItem.find({"cartId":req.params.name},["productName","amount","totalPrice","cartId"],(err,data)=>{
    var filename = new Date().getTime().toString();
    var header="hello  :"+data[0].cartId+"\n"
    fs.appendFile(__dirname+"/reports/"+filename+".txt",header,encoding='utf8',err=>{
    })
                   for(let i=0;i<data.length;i++){
                         let product="product  :"+data[i].productName;
                         let amount="amount  :"+data[i].amount;
                         let totalPrice="total price  :"+data[i].totalPrice;
                         let arr=[product,amount,totalPrice];
    
                         fs.appendFile(__dirname+"/reports/"+filename+".txt",arr+"\n",encoding='utf8',err=>{
                         })
                     }//for loop
                       res.download(__dirname+"/reports/"+filename+".txt",filename+".txt");
    return next();
    
     })//cart find
    
    
    }
  module.exports={
                  findOrder:findOrder,
                  numorders:numorders,
                  createOrderFile:createOrderFile,
                  checkDate:checkDate,
                  allorders:allorders,
                  order:order

                 }