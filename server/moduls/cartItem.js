const mongoose=require("mongoose");
const express = require('express');
const dateFormat = require('dateformat');
const Schema=mongoose.Schema;



const cartItemSchema=new Schema({
    productName:String,
    amount:Number,
    totalPrice:Number,
    cartId:String,
    cartItemPic:String
 })
 const cartItem=mongoose.model("cartItems",cartItemSchema);
 
 const itemInCart=(req,res,next)=>{
      res.json(req.body.newCartItem)
      let amount=req.body.newCartItem.amount;
      let price=req.body.newCartItem.price;
   const cart1=new cartItem({
     productName :req.body.newCartItem.productName,
     amount    :req.body.newCartItem.amount,
     totalPrice:amount*price,
     cartId    :req.body.newCartItem.userid,
     cartItemPic   :req.body.newCartItem.picProduct
   })
   cart1.save();
    
 }

 const sumOfItemsCart=(req,res,next)=>{
  
  // cartItem.aggregate([
  //    {$smatch:{cartId : req.params.name}},
  //    {$group:{
  //       totalPrice:{$sum: 'totalPrice'}
  //    }}
  // ],(err,result)=>{
  //   console.log(result)
  //   res.json(result)
    
  // })
  // next();

  let sum=0;
  cartItem.find({"cartId":req.params.name},(err,data)=>{
      for (let i = 0; i < data.length; i++) {
        if(data[i].totalPrice!=undefined){
          sum+=data[i].totalPrice;
        }
      }
      
      res.json(Math.round(sum * 100) / 100);
  })
    next();
 }

 const removeItemFromCart=(req,res,next)=>{
    cartItem.remove({"_id":req.params.id},(err,data)=>{
      if (err){
        return err
      }
    })
   res.json(req.params.id);
  return next();
  }

  const delAllFromCart=(req,res,next)=>{
    // console.log("GFSDFGS"+req.body.userId);
    // console.log("bbbbb"+req.params.user)
     cartItem.remove({"cartId":req.params.user},(err,data)=>{
      if (err){
        return err
      }
    })
    res.json(req.params.user)
     return next()
  }
  const delAllAfterOrder=(req,res,next)=>{
    // console.log("GFSDFGS"+req.body.userId);
    // console.log("bbbbb"+req.params.user)
     cartItem.remove({"cartId":req.body.userId},(err,data)=>{
      if (err){
        return err
      }
    })
    // res.json(req.params.user)
     return next()
  }



  
  const getCart=(req,res,next)=>{
    cartItem.find({"cartId":req.params.name},(err,data)=>{
      res.json(data);
    })
    next();
  }

  
  


  module.exports={
                 cartItem:cartItem,
                 removeItemFromCart:removeItemFromCart,
                 delAllFromCart:delAllFromCart,
                 getCart:getCart ,
                 itemInCart:itemInCart,
                 sumOfItemsCart:sumOfItemsCart,
                 delAllAfterOrder:delAllAfterOrder
                 
                }